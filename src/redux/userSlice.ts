import { createSlice } from '@reduxjs/toolkit';
import { IUserState } from '../ts/interfaces';

const initialState: IUserState = {
  currentUser: null,
  isFetching: false,
  error: false,
};
const userSlice = createSlice({
  name: 'user',
  initialState: initialState,

  reducers: {
    loginStart: (state) => {
      state.isFetching = true;
    },
    loginSuccess: (state, action) => {
      state.isFetching = false;
      state.currentUser = action.payload;
      state.error = false;
    },
    loginFailure: (state) => {
      state.isFetching = false;
      state.error = true;
    },
    logOut: (state) => {
      state.currentUser = null;
      state.isFetching = false;
      state.error = false;
    },
  },
});

export const { loginFailure, loginStart, loginSuccess, logOut } =
  userSlice.actions;
export default userSlice.reducer;
