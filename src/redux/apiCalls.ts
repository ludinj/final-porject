import { Dispatch } from '@reduxjs/toolkit';
import axios from 'axios';

import { loginFailure, loginStart } from './userSlice';

import { EFilters, ESection } from '../ts/enums';
import { ICurrentUser, IMovie, ITvShow } from '../ts/interfaces';
import { DEPLOY_URL } from '../utils/constants';

const axiosInstance = axios.create();

axiosInstance.interceptors.request.use((config) => {
  config.params = config.params || {};
  config.params[EFilters.api_key] = `${process.env.REACT_APP_API_KEY}`;
  return config;
});

export const login = async (dispatch: Dispatch) => {
  dispatch(loginStart());
  try {
    const data = await getRequestToken();
    window.location.href = `https://www.themoviedb.org/authenticate/${data.request_token}?redirect_to=${DEPLOY_URL}`;
  } catch (error) {
    dispatch(loginFailure());
  }
};
const getRequestToken = async () => {
  try {
    const response = await axiosInstance.get(
      `${process.env.REACT_APP_BASE_API_URL}/3/authentication/token/new`
    );

    return response.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
export const getSession = async (requestToken: string) => {
  try {
    const response = await axiosInstance.post(
      `${process.env.REACT_APP_BASE_API_URL}/3/authentication/session/new`,
      {
        request_token: requestToken,
      }
    );
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const addToFavoritesMovie = async (
  currentUser: ICurrentUser,
  movie: IMovie
) => {
  const response = await axiosInstance.post(
    `${process.env.REACT_APP_BASE_API_URL}/3/account/${currentUser.id}/favorite?session_id=${currentUser.sessionId}`,
    { media_type: ESection.movie, media_id: movie.id, favorite: true }
  );
  return response.data;
};

export const addToFavoritesShows = async (
  currentUser: ICurrentUser,
  show: ITvShow
) => {
  const response = await axiosInstance.post(
    `${process.env.REACT_APP_BASE_API_URL}/3/account/${currentUser.id}/favorite?session_id=${currentUser.sessionId}`,
    { media_type: ESection.tv, media_id: show.id, favorite: true }
  );
  return response.data;
};

export const deleteToFavoritesMovie = async (
  currentUser: ICurrentUser,
  movie: IMovie
) => {
  const response = await axiosInstance.post(
    `${process.env.REACT_APP_BASE_API_URL}/3/account/${currentUser.id}/favorite?session_id=${currentUser.sessionId}`,
    { media_type: ESection.movie, media_id: movie.id, favorite: false }
  );
  return response.data;
};
export const deleteToFavoriteShow = async (
  currentUser: ICurrentUser,
  show: ITvShow
) => {
  const response = await axiosInstance.post(
    `${process.env.REACT_APP_BASE_API_URL}/3/account/${currentUser.id}/favorite?session_id=${currentUser.sessionId}`,
    { media_type: ESection.tv, media_id: show.id, favorite: false }
  );
  return response.data;
};
