import { Outlet } from 'react-router-dom';

import Navbar from '../components/navbar/navbar';
import './layout.scss';
const Layout = () => {
  return (
    <div className='layout'>
      <Navbar />
      <div className='outlet-container'>
        <Outlet />
      </div>
    </div>
  );
};

export default Layout;
