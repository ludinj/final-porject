import React, { useEffect, useRef, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { EPages } from '../../ts/enums';
import { MdLocalMovies } from 'react-icons/md';
import { getSession, login } from '../../redux/apiCalls';
import { useDispatch, useSelector } from 'react-redux';
import { getQueryVariable } from '../../utils/getQueryVariable';
import { loginSuccess } from '../../redux/userSlice';
import { RootState } from '../../redux/store';
import './navbar.scss';
import { useFetch } from '../../hooks/useFetch';
import { ImMenu } from 'react-icons/im';
import UserMenu from '../user-menu/user-menu';
const Navbar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { fetchData } = useFetch();
  const { currentUser } = useSelector((state: RootState) => state.user);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const renderAfterCalled = useRef(false);
  useEffect(() => {
    const getSessionId = async () => {
      try {
        const requestToken = getQueryVariable('request_token') || '';
        const approved = getQueryVariable('approved')
          ? Boolean(getQueryVariable('approved'))
          : false;
        if (approved && !renderAfterCalled.current) {
          const { session_id: sessionId } = await getSession(requestToken);
          const userResponse = await fetchData(
            `${process.env.REACT_APP_BASE_API_URL}/3/account?session_id=${sessionId}`
          );
          const user = { ...userResponse, sessionId: sessionId };
          dispatch(loginSuccess(user));
          window.history.pushState({}, document.title, '/');
        }
      } catch (error) {
        alert(error);
      }
    };

    getSessionId();
    renderAfterCalled.current = true;
  }, [dispatch, fetchData]);

  const handleClickLogo = () => {
    navigate(EPages.home);
  };
  return (
    <div className='navbar'>
      <div className='navbar__container'>
        <div className='logo' onClick={handleClickLogo}>
          <MdLocalMovies />
        </div>
        <nav className='navigation'>
          <div className='links'>
            <NavLink
              className={({ isActive }) => (isActive ? 'active-link' : '')}
              to={`/${EPages.movies}`}
            >
              Movies
            </NavLink>
            <NavLink
              className={({ isActive }) => (isActive ? 'active-link' : '')}
              to={`/${EPages.tv}`}
            >
              Tv Shows
            </NavLink>
            <NavLink
              className={({ isActive }) => (isActive ? 'active-link' : '')}
              to={`/${EPages.search}`}
            >
              Search
            </NavLink>
          </div>
          {!currentUser ? (
            <div className='user-login' onClick={() => login(dispatch)}>
              Login
            </div>
          ) : (
            <div
              className='user-login'
              onClick={() => setOpenModal((pre) => !pre)}
            >
              <p>{currentUser.username}</p>
              {openModal ? <UserMenu /> : null}
            </div>
          )}
        </nav>
        <div
          className='hamburger-button'
          onClick={() => setOpenModal((pre) => !pre)}
        >
          <ImMenu />
          {openModal ? <UserMenu /> : null}
        </div>
      </div>
    </div>
  );
};

export default Navbar;
