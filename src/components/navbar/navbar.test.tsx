import { render, screen, waitFor } from '@testing-library/react';
import Navbar from './navbar';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import userEvent from '@testing-library/user-event';
import { loginSuccess, logOut } from '../../redux/userSlice';
import { mockUser } from '../../mockData/mockUser';

export const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element}
        <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}

describe('<Navbar/>', () => {
  beforeEach(() => {
    store.dispatch(logOut());
  });
  test('Should navigate to movies page', async () => {
    renderWithContext(<Navbar />);
    const moviesLink = screen.getByText('Movies');

    userEvent.click(moviesLink);
    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        '/movies'
      );
    });
  });
  test('Should navigate to Tv Shows page', async () => {
    renderWithContext(<Navbar />);
    const showsLink = screen.getByText('Tv Shows');

    userEvent.click(showsLink);
    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent('/tv');
    });
  });
  test('Should render username', async () => {
    store.dispatch(loginSuccess(mockUser));
    renderWithContext(<Navbar />);
    const showsLink = screen.getByText('Tv Shows');

    userEvent.click(showsLink);
    await waitFor(() => {
      expect(screen.getByText(/ludinj/i)).toBeInTheDocument();
    });
  });
});
