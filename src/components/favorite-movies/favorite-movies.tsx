import React, { useState, useEffect } from 'react';
import {
  BsFillArrowLeftSquareFill,
  BsFillArrowRightSquareFill,
} from 'react-icons/bs';
import ReactPaginate from 'react-paginate';
import { useSelector } from 'react-redux';
import { useChangeParams } from '../../hooks/useChangeParams';
import { useFetch } from '../../hooks/useFetch';
import { deleteToFavoritesMovie } from '../../redux/apiCalls';
import { RootState } from '../../redux/store';
import { IMovie } from '../../ts/interfaces';
import MovieCard from '../movie-card/movie-card';
import './favorite-movies.scss';

const FavoriteMovies = () => {
  const [totalPages, setTotalPages] = useState<number | undefined>(undefined);
  const [favoriteMovies, setFavoriteMovies] = useState<IMovie[]>([]);
  const { currentUser } = useSelector((state: RootState) => state.user);
  const { fetchData, loading } = useFetch();
  const {
    handlePageClick,

    currentPage,
  } = useChangeParams();

  useEffect(() => {
    const getFavoriteMovies = async () => {
      const favoriteMoviesResponse = await fetchData(
        `${process.env.REACT_APP_BASE_API_URL}/3/account/${currentUser?.id}/favorite/movies?session_id=${currentUser?.sessionId}&page=${currentPage}`
      );
      setFavoriteMovies(favoriteMoviesResponse.results);
      setTotalPages(favoriteMoviesResponse.totalPages);
    };
    getFavoriteMovies();
  }, [fetchData, currentUser, currentPage]);

  const handleDelete = async (
    movie: IMovie,
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    if (!currentUser) return;

    if (window.confirm('Delete Movie?')) {
      setFavoriteMovies((pre) =>
        pre.filter((pre) => {
          return pre.id !== movie.id;
        })
      );
      await deleteToFavoritesMovie(currentUser, movie);
    } else {
      return;
    }
  };
  return (
    <div className='favorite-movies'>
      {!loading ? (
        <div className='favorites-gird'>
          {favoriteMovies.length > 0 ? (
            favoriteMovies.map((movie: IMovie, idx) => {
              return (
                <div className='card-container' key={idx}>
                  <button
                    onClick={(e) => handleDelete(movie, e)}
                    className='card-container__button'
                    data-testid={`button-${idx}`}
                  >
                    Delete
                  </button>
                  <MovieCard movie={movie} key={movie.id} />
                </div>
              );
            })
          ) : (
            <h3>No Results</h3>
          )}
        </div>
      ) : (
        <h3>Loading...</h3>
      )}
      {totalPages ? (
        <ReactPaginate
          className='pagination'
          breakLabel='...'
          nextLabel={<BsFillArrowRightSquareFill />}
          onPageChange={(e) => handlePageClick(e)}
          pageRangeDisplayed={5}
          pageCount={totalPages}
          previousLabel={<BsFillArrowLeftSquareFill />}
          pageClassName='page'
          activeClassName='active-page'
          forcePage={parseInt(currentPage) - 1}
          disabledClassName='disableButton'
        />
      ) : null}
    </div>
  );
};

export default FavoriteMovies;
