import { render, screen, waitFor } from '@testing-library/react';
import FavoriteMovies from './favorite-movies';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import { loginSuccess, logOut } from '../../redux/userSlice';
import { mockUser } from '../../mockData/mockUser';

import userEvent from '@testing-library/user-event';
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}
describe('<FavoriteMovies/>', () => {
  beforeEach(() => {
    store.dispatch(logOut());
  });
  test('Should render the  favorites movies details', async () => {
    store.dispatch(loginSuccess(mockUser));
    renderWithContext(<FavoriteMovies />);

    await waitFor(() => {
      expect(screen.getByText(/Black Adam/i)).toBeInTheDocument();
    });
  });
  test('Should delete a movie', async () => {
    store.dispatch(loginSuccess(mockUser));
    jest.spyOn(window, 'confirm').mockImplementation(() => true);
    renderWithContext(<FavoriteMovies />);
    await waitFor(() => {
      expect(screen.getByText(/Black Adam/i)).toBeInTheDocument();
    });

    const button = screen.getByTestId(`button-0`);
    userEvent.click(button);
    await waitFor(() => {
      expect(screen.queryByText(/Black Adam/i)).not.toBeInTheDocument();
    });
  });
});
