import { render, screen, waitFor } from '@testing-library/react';
import MovieCredits from './movie-credits';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import { ICurrentUser } from '../../ts/interfaces';
import userEvent from '@testing-library/user-event';
import { loginSuccess, logOut } from '../../redux/userSlice';
import { mockCredits } from '../../mockData/mockCredits';

const mockUser: ICurrentUser = {
  username: 'ludinj',
  id: 1,
  avatar: { gravatar: { hash: '' }, tmdb: {} },
  includeAdult: false,
  sessionId: '13',
  name: 'ludin',
};

export const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element}
        <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}

describe('<MovieCredits/>', () => {
  test('Should render cast and crew', async () => {
    renderWithContext(<MovieCredits credits={mockCredits} />);

    await waitFor(() => {
      expect(screen.getAllByText(/-Dwayne Johnson/i).length).toBeGreaterThan(1);
    });
    await waitFor(() => {
      expect(screen.getByText(/Harry Cohen/i)).toBeInTheDocument();
    });
  });
});
