import React from 'react';
import { ICredits } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';
import { EPages } from '../../ts/enums';
import { maxCreditsResults } from '../../utils/constants';
import './movie-credits.scss';
type MovieCreditsProps = {
  credits: ICredits | null;
};
const MovieCredits = ({ credits }: MovieCreditsProps) => {
  const navigate = useNavigate();

  return (
    <div className='movie-credits'>
      <h1>Credits.</h1>

      <div className='credits__container'>
        <div className='cast'>
          <h2>Cast:</h2>
          <div>
            {credits?.cast.slice(0, maxCreditsResults).map((actor) => {
              return (
                <div
                  className='cast-card'
                  key={actor.id}
                  onClick={() => navigate(`/${EPages.person}/${actor.id}`)}
                >
                  <p>-{actor.name}</p>
                  <p>As: {actor.character}</p>
                </div>
              );
            })}
          </div>
        </div>
        <div className='crew'>
          <h2>Crew:</h2>
          <div className=''>
            {credits?.crew.slice(0, maxCreditsResults).map((crew, idx) => {
              return (
                <div
                  className='cast-card'
                  key={idx}
                  onClick={() => navigate(`/${EPages.person}/${crew.id}`)}
                >
                  <p>-{crew.name}</p>
                  <p>Department: {crew.department}</p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default MovieCredits;
