import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { RootState } from '../../redux/store';
import { EPages } from '../../ts/enums';
import { login } from '../../redux/apiCalls';

import './user-menu.scss';
import { useDispatch } from 'react-redux';
import { logOut } from '../../redux/userSlice';
const UserMenu = () => {
  const { currentUser } = useSelector((state: RootState) => state.user);
  const dispatch = useDispatch();

  return (
    <div className='user-menu'>
      <Link to={EPages.movies}>Movies</Link>
      <Link to={EPages.tv}>Tv Shows</Link>
      <Link to={EPages.search}>Search</Link>
      {currentUser ? (
        <Link to={EPages.userDetails}>User Information</Link>
      ) : (
        <button onClick={() => login(dispatch)}>Login</button>
      )}
      {currentUser ? (
        <button onClick={() => dispatch(logOut())}>Sing out</button>
      ) : null}
    </div>
  );
};

export default UserMenu;
