import { render, screen } from '@testing-library/react';
import SimilarShows from './similar-shows';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

import { mockTvShows } from '../../mockData/mockTvShows';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<SimilarShows/>', () => {
  test('Should render the first show', () => {
    renderWithContext(<SimilarShows shows={mockTvShows.results} />);
    expect(screen.getByText('House of the Dragon')).toBeInTheDocument();
  });
  test('Should render the  second show', () => {
    renderWithContext(<SimilarShows shows={mockTvShows.results} />);
    expect(
      screen.getByText('Dahmer – Monster: The Jeffrey Dahmer Story')
    ).toBeInTheDocument();
  });
  test('Should render no shows', () => {
    renderWithContext(<SimilarShows shows={[]} />);
    expect(screen.getByText('No Results')).toBeInTheDocument();
  });
});
