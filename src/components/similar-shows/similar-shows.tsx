import { ITvShow } from '../../ts/interfaces';
import TvCard from '../TvCard/tv-card';
import './similar-shows.scss';
type SimilarShowsProps = {
  shows: ITvShow[];
};
const SimilarShows = ({ shows }: SimilarShowsProps) => {
  return (
    <div className='similar-shows'>
      <h1>Similar shows.</h1>
      <div className='similar-shows__gird'>
        {shows.length > 0 ? (
          shows.map((show: ITvShow) => {
            return <TvCard show={show} key={show.id} />;
          })
        ) : (
          <h3>No Results</h3>
        )}
      </div>
    </div>
  );
};

export default SimilarShows;
