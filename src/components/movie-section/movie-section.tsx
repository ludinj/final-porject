import React, { useEffect, useState } from 'react';
import { useFetch } from '../../hooks/useFetch';
import MovieCard from '../movie-card/movie-card';
import { ICertification, IMovie } from '../../ts/interfaces';
import ReactPaginate from 'react-paginate';
import {
  BsFillArrowRightSquareFill,
  BsFillArrowLeftSquareFill,
} from 'react-icons/bs';
import GenresSection from '../genres-section/genres-section';
import { generateYears } from '../../utils/generateYears';
import { useChangeParams } from '../../hooks/useChangeParams';
import './movie-section.scss';
import { maxPages } from '../../utils/constants';
import { AiFillHeart } from 'react-icons/ai';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import { addToFavoritesMovie } from '../../redux/apiCalls';
import { useNavigate } from 'react-router-dom';
const MovieSection = () => {
  const { fetchData } = useFetch();
  const [movies, setMovies] = useState<IMovie[] | null>(null);
  const [totalPages, setTotalPages] = useState<number | undefined>(undefined);
  const [certifications, setCertifications] = useState<
    ICertification[] | undefined
  >(undefined);
  const { currentUser } = useSelector((state: RootState) => state.user);
  const navigate = useNavigate();
  const {
    handlePageClick,
    handleYearChange,
    handleChangeCertification,
    yearQuery,
    currentPage,
    genreQuery,
    certificationQuery,
  } = useChangeParams();
  const years = generateYears();

  useEffect(() => {
    const getMovies = async () => {
      try {
        const moviesResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/discover/movie?page=${currentPage}&with_genres=${genreQuery}&primary_release_year=${yearQuery}&certification_country=US&certification=${certificationQuery}`
        );

        if (!moviesResponse) {
          navigate('/*');
          return;
        }
        if (moviesResponse.totalPages > maxPages) {
          setTotalPages(maxPages);
        } else {
          setTotalPages(moviesResponse.totalPages);
        }
        setMovies(moviesResponse.results);
      } catch (error) {
        alert(error);
      }
    };
    getMovies();
  }, [
    fetchData,
    currentPage,
    genreQuery,
    yearQuery,
    certificationQuery,
    navigate,
  ]);

  useEffect(() => {
    const getCertifications = async () => {
      try {
        const certificationsResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/certification/movie/list`
        );

        setCertifications(certificationsResponse.certifications.us);
      } catch (error) {
        alert(error);
      }
    };
    getCertifications();
  }, [fetchData]);

  const handleAddToFavorites = async (
    movie: IMovie,
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    if (currentUser) {
      try {
        await addToFavoritesMovie(currentUser, movie);
        alert('Movie Added to favorites');
      } catch (error) {
        alert(error);
      }
    } else {
      return;
    }
  };
  return (
    <div className='movie-section'>
      <div className='filters'>
        <GenresSection media='movie' />
        <div className='selects'>
          <div className='select-item'>
            <label htmlFor='years'>By Year:</label>
            <select
              name='years'
              id='years'
              onChange={(e) => handleYearChange(e)}
            >
              <option value=''>All</option>
              {years.map((year, idx) => {
                return (
                  <option value={year} key={idx}>
                    {year}
                  </option>
                );
              })}
            </select>
          </div>
          <div className='select-item'>
            <label htmlFor='certification'>By Certification:</label>
            <select
              name='certification'
              id='certification'
              onChange={(e) => handleChangeCertification(e)}
            >
              <option value=''>All</option>
              {certifications?.map((certification, idx) => {
                return (
                  <option value={certification.certification} key={idx}>
                    {certification.certification}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
      </div>

      {movies ? (
        <div className='movies-gird'>
          {movies.length > 0 ? (
            movies.map((movie: IMovie, idx) => {
              return (
                <div className='card-container' key={idx}>
                  {currentUser ? (
                    <button
                      onClick={(e) => handleAddToFavorites(movie, e)}
                      className='card-container__button'
                    >
                      <AiFillHeart size={20} />
                    </button>
                  ) : null}
                  <MovieCard movie={movie} />
                </div>
              );
            })
          ) : (
            <h3>No Results</h3>
          )}
        </div>
      ) : (
        <h3>Loading...</h3>
      )}
      {totalPages ? (
        <ReactPaginate
          className='pagination'
          breakLabel='...'
          nextLabel={<BsFillArrowRightSquareFill />}
          onPageChange={(e) => handlePageClick(e)}
          pageRangeDisplayed={5}
          pageCount={totalPages}
          previousLabel={<BsFillArrowLeftSquareFill />}
          pageClassName='page'
          activeClassName='active-page'
          forcePage={parseInt(currentPage) - 1}
          disabledClassName='disableButton'
        />
      ) : null}
    </div>
  );
};

export default MovieSection;
