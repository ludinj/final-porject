import React from 'react';
import { IMedia } from '../../ts/interfaces';
import './media-card.scss';
import noImage from '../../images/noImage.png';
import { ESection } from '../../ts/enums';
import MovieCard from '../movie-card/movie-card';
import TvCard from '../TvCard/tv-card';

type MediaCardProps = {
  media: IMedia;
};
const MediaCard = ({ media }: MediaCardProps) => {
  if (media.mediaType === ESection.movie) {
    return <MovieCard movie={media} />;
  } else {
    return <TvCard show={media} />;
  }
};

export default MediaCard;
