import { render, screen } from '@testing-library/react';

import SeasonSection from './seasons-section';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

import { mockSeasons } from '../../mockData/mockSeasons';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<SeasonSection/>', () => {
  test('Should all the seasons', () => {
    renderWithContext(<SeasonSection seasons={mockSeasons} />);
    expect(screen.getByText('Season: 1')).toBeInTheDocument();
    expect(screen.getByText('Season: 11')).toBeInTheDocument();
  });
});
