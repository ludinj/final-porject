import React from 'react';
import { ISeason } from '../../ts/interfaces';
import SeasonCard from '../season-card/season-card';

import './seasons-section.scss';

type SeasonSectionProps = {
  seasons: ISeason[] | undefined;
};
const SeasonSection = ({ seasons }: SeasonSectionProps) => {
  return (
    <div className='seasons-section'>
      <h1>Seasons.</h1>

      <div className='seasons__container'>
        {seasons?.map((season, idx) => {
          return <SeasonCard season={season} key={idx} />;
        })}
      </div>
    </div>
  );
};

export default SeasonSection;
