import React from 'react';
import { IMovie } from '../../ts/interfaces';
import MovieCard from '../movie-card/movie-card';

import './similar-movies.scss';

type SimilarMoviesProps = {
  movies: IMovie[];
};
const SimilarMovies = ({ movies }: SimilarMoviesProps) => {
  return (
    <div className='similar-movies'>
      <h1>Similar movies.</h1>
      <div className='similar-movies__gird'>
        {movies.length > 0 ? (
          movies.map((movie: IMovie) => {
            return <MovieCard movie={movie} key={movie.id} />;
          })
        ) : (
          <h3>No Results</h3>
        )}
      </div>
    </div>
  );
};

export default SimilarMovies;
