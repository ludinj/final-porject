import { render, screen, waitFor } from '@testing-library/react';
import SimilarMovies from './similar-movies';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

import { ICurrentUser } from '../../ts/interfaces';

import { mockMovies } from '../../mockData/mockMovies';

const mockUser: ICurrentUser = {
  username: 'ludinj',
  id: 1,
  avatar: { gravatar: { hash: '' }, tmdb: {} },
  includeAdult: false,
  sessionId: '13',
  name: 'ludin',
};
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<SimilarMovies/>', () => {
  test('Should render the first movie', () => {
    renderWithContext(<SimilarMovies movies={mockMovies.results} />);
    expect(screen.getByText('Terrifier 2')).toBeInTheDocument();
  });
  test('Should render the  second movie', () => {
    renderWithContext(<SimilarMovies movies={mockMovies.results} />);
    expect(screen.getByText('Black Adam')).toBeInTheDocument();
  });
  test('Should render no movies', () => {
    renderWithContext(<SimilarMovies movies={[]} />);
    expect(screen.getByText('No Results')).toBeInTheDocument();
  });
});
