import React, { useState, useEffect } from 'react';
import {
  BsFillArrowLeftSquareFill,
  BsFillArrowRightSquareFill,
} from 'react-icons/bs';
import ReactPaginate from 'react-paginate';
import { useSelector } from 'react-redux';
import { useChangeParams } from '../../hooks/useChangeParams';
import { useFetch } from '../../hooks/useFetch';
import { deleteToFavoriteShow } from '../../redux/apiCalls';
import { RootState } from '../../redux/store';
import { ITvShow } from '../../ts/interfaces';
import TvCard from '../TvCard/tv-card';
import './favorite-shows.scss';

const FavoriteShows = () => {
  const [totalPages, setTotalPages] = useState<number | undefined>(undefined);
  const [favoriteShows, setFavoriteShows] = useState<ITvShow[]>([]);
  const { currentUser } = useSelector((state: RootState) => state.user);
  const { fetchData, loading } = useFetch();
  const { handlePageClick, currentPage } = useChangeParams();

  useEffect(() => {
    const getFavoriteMovies = async () => {
      const favoriteShowsResponse = await fetchData(
        `${process.env.REACT_APP_BASE_API_URL}/3/account/${currentUser?.id}/favorite/tv?session_id=${currentUser?.sessionId}&page=${currentPage}`
      );

      setFavoriteShows(favoriteShowsResponse.results);
      setTotalPages(favoriteShowsResponse.totalPages);
    };
    getFavoriteMovies();
  }, [fetchData, currentUser, currentPage]);

  const handleDelete = async (
    show: ITvShow,
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    if (!currentUser) return;
    if (window.confirm('Delete show?')) {
      setFavoriteShows((pre) =>
        pre.filter((pre) => {
          return pre.id !== show.id;
        })
      );
      await deleteToFavoriteShow(currentUser, show);
    } else {
      return;
    }
  };
  return (
    <div className='favorite-shows'>
      {!loading ? (
        <div className='favorites-gird'>
          {favoriteShows.length > 0 ? (
            favoriteShows.map((show: ITvShow, idx) => {
              return (
                <div className='card-container' key={idx}>
                  <button
                    onClick={(e) => handleDelete(show, e)}
                    className='card-container__button'
                    data-testid={`button-${idx}`}
                  >
                    Delete
                  </button>
                  <TvCard show={show} key={show.id} />
                </div>
              );
            })
          ) : (
            <h3>No Results</h3>
          )}
        </div>
      ) : (
        <h3>Loading...</h3>
      )}
      {totalPages ? (
        <ReactPaginate
          className='pagination'
          breakLabel='...'
          nextLabel={<BsFillArrowRightSquareFill />}
          onPageChange={(e) => handlePageClick(e)}
          pageRangeDisplayed={5}
          pageCount={totalPages}
          previousLabel={<BsFillArrowLeftSquareFill />}
          pageClassName='page'
          activeClassName='active-page'
          forcePage={parseInt(currentPage) - 1}
          disabledClassName='disableButton'
        />
      ) : null}
    </div>
  );
};
export default FavoriteShows;
