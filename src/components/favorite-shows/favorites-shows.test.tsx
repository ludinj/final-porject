import { render, screen, waitFor } from '@testing-library/react';
import FavoriteShows from './favorite-shows';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import { loginSuccess, logOut } from '../../redux/userSlice';
import { mockUser } from '../../mockData/mockUser';

import userEvent from '@testing-library/user-event';
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}
describe('<FavoriteShows/>', () => {
  beforeEach(() => {
    store.dispatch(logOut());
  });
  test('Should render the  favorites shows', async () => {
    store.dispatch(loginSuccess(mockUser));
    renderWithContext(<FavoriteShows />);

    await waitFor(() => {
      expect(screen.getByText(/The Walking Dead/i)).toBeInTheDocument();
    });
  });
  test('Should delete a movie', async () => {
    store.dispatch(loginSuccess(mockUser));
    jest.spyOn(window, 'confirm').mockImplementation(() => true);
    renderWithContext(<FavoriteShows />);
    await waitFor(() => {
      expect(screen.getByText(/The Walking Dead/i)).toBeInTheDocument();
    });

    const button = screen.getByTestId(`button-0`);
    userEvent.click(button);
    await waitFor(() => {
      expect(screen.queryByText(/The Walking Dead/i)).not.toBeInTheDocument();
    });
  });
});
