import { render, screen, waitFor } from '@testing-library/react';
import TvCard from './tv-card';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

import { mockTvShows } from '../../mockData/mockTvShows';
import userEvent from '@testing-library/user-event';

export const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element} <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}

describe('<TvCard/>', () => {
  test('Should render a tv card', () => {
    renderWithContext(<TvCard show={mockTvShows.results[0]} />);
    expect(screen.getByText('House of the Dragon')).toBeInTheDocument();
  });

  test('Should navigate to tv details on click', async () => {
    renderWithContext(<TvCard show={mockTvShows.results[0]} />);
    expect(screen.getByText('House of the Dragon')).toBeInTheDocument();
    const container = screen.getByTestId(mockTvShows.results[0].id);
    userEvent.click(container);

    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        `/tv/${mockTvShows.results[0].id}`
      );
    });
  });
});
