import React from 'react';
import { useNavigate } from 'react-router-dom';
import { EPages } from '../../ts/enums';
import { ITvShow } from '../../ts/interfaces';
import noImage from '../../images/noImage.png';
import './tv-card.scss';
import { imageBaseUrl } from '../../utils/constants';

type MovieCardProps = {
  show: ITvShow;
};
const TvCard = ({ show }: MovieCardProps) => {
  const navigate = useNavigate();
  const date = show.firstAirDate
    ? new Date(show.firstAirDate).toDateString()
    : '';
  const image = `${imageBaseUrl}/t/p/w500/${show.posterPath}`;

  const handleClick = () => {
    navigate(`/${EPages.tv}/${show.id}`);
  };
  return (
    <div className='tv-card' onClick={handleClick} data-testid={show.id}>
      <img
        src={show.posterPath ? image : noImage}
        alt={show.name}
        loading='lazy'
      />
      <div className='tv-card__info'>
        <h4>{show.name}</h4>
        <p>{date}</p>
        <p>Tv show</p>
      </div>
    </div>
  );
};

export default TvCard;
