import { render, screen } from '@testing-library/react';
import Reviews from './reviews';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

import { mockReviews } from '../../mockData/mockReviews';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<Reviews/>', () => {
  test('Should render the reviews', async () => {
    renderWithContext(<Reviews reviews={mockReviews} />);
    expect(screen.getByText(/shamxal/i)).toBeInTheDocument();
  });
  test('Should render no reviews', async () => {
    renderWithContext(<Reviews reviews={[]} />);
    expect(screen.getByText(/no reviews yet/i)).toBeInTheDocument();
  });
});
