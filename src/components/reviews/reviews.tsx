import React from 'react';
import { IReview } from '../../ts/interfaces';
import { maxReviewParagraphs } from '../../utils/constants';
import './reviews.scss';

type ReviewsProps = {
  reviews: IReview[];
};
const Reviews = ({ reviews }: ReviewsProps) => {
  const reviewContent = reviews.map((review) => {
    return {
      ...review,
      content: review.content
        .split('.')
        .slice(0, maxReviewParagraphs)
        .join('. '),
    };
  });

  return (
    <div className='reviews'>
      <h1>Reviews.</h1>
      {reviews.length > 0 ? (
        <>
          {reviewContent.map((review, idx) => {
            return (
              <div className='review' key={idx}>
                <h4>-{review.author}.</h4>
                <p>{review.content}.</p>
              </div>
            );
          })}
        </>
      ) : (
        <h3>No reviews yet</h3>
      )}
    </div>
  );
};

export default Reviews;
