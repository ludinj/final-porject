import { render, screen, waitFor } from '@testing-library/react';
import MovieInfo from './movie-info';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import { mockMovies } from '../../mockData/mockMovies';
import userEvent from '@testing-library/user-event';

import { ICurrentUser, IUserState } from '../../ts/interfaces';
import { loginSuccess, logOut } from '../../redux/userSlice';

const mockUser: ICurrentUser = {
  username: 'ludinj',
  id: 1,
  avatar: { gravatar: { hash: '' }, tmdb: {} },
  includeAdult: false,
  sessionId: '13',
  name: 'ludin',
};
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<MovieInfo/>', () => {
  beforeEach(() => {
    store.dispatch(logOut());
  });
  test('Should render the movie info', () => {
    renderWithContext(<MovieInfo movie={mockMovies.results[0]} />);
    expect(screen.getByText('Terrifier 2')).toBeInTheDocument();
  });
  test('Should add movie to favorites', async () => {
    store.dispatch(loginSuccess(mockUser));
    renderWithContext(<MovieInfo movie={mockMovies.results[0]} />);
    const button = screen.getByText('Add to Favorites');
    userEvent.click(button);
    jest.spyOn(window, 'alert').mockImplementation();
    await waitFor(() => {
      expect(window.alert).toBeCalledWith('Movie Added to favorites');
    });
  });
  test('Should not have a add to favorites button', async () => {
    renderWithContext(<MovieInfo movie={mockMovies.results[0]} />);
    expect(screen.queryByText('Add to Favorites')).not.toBeInTheDocument();
  });
});
