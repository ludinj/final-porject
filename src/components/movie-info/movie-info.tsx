import React from 'react';
import { useSelector } from 'react-redux';
import { addToFavoritesMovie } from '../../redux/apiCalls';
import { RootState } from '../../redux/store';
import { IMovie } from '../../ts/interfaces';
import { imageBaseUrl } from '../../utils/constants';
import noImage from '../../images/noImage.png';

import './movie-info.scss';
type MovieInfoProps = {
  movie: IMovie | null;
};
const MovieInfo = ({ movie }: MovieInfoProps) => {
  const image = `${imageBaseUrl}/t/p/original/${movie?.backdropPath}`;
  const { currentUser } = useSelector((state: RootState) => state.user);

  const handleAddToFavorites = async () => {
    if (currentUser && movie) {
      try {
        await addToFavoritesMovie(currentUser, movie);
        alert('Movie Added to favorites');
      } catch (error) {
        alert(error);
      }
    } else {
      return;
    }
  };
  return (
    <div className='movie-info'>
      <div className='movie-info__image'>
        <img src={movie?.backdropPath ? image : noImage} alt={movie?.title} />
      </div>
      <div className='info'>
        <h2>{movie?.title}</h2>
        <p>{movie?.overview}</p>

        <div className='info-item'>
          <h4>Rating:</h4>
          <p> {movie?.voteAverage}</p>
        </div>
        <div className='info-item'>
          <h4>Genres:</h4>
          {movie?.genres.map((genre, idx) => {
            return <p key={idx}> {genre.name}.</p>;
          })}
        </div>

        <div className='info-item'>
          <h4> Release date: </h4>
          <p>{movie?.releaseDate}</p>
        </div>
        {currentUser ? (
          <button className='favorite-button' onClick={handleAddToFavorites}>
            Add to Favorites
          </button>
        ) : null}
      </div>
    </div>
  );
};

export default MovieInfo;
