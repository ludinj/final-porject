import { render, screen, waitFor } from '@testing-library/react';
import ShowInfo from './show-info';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import userEvent from '@testing-library/user-event';
import { ICurrentUser } from '../../ts/interfaces';
import { loginSuccess, logOut } from '../../redux/userSlice';
import { mockTvShows } from '../../mockData/mockTvShows';

const mockUser: ICurrentUser = {
  username: 'ludinj',
  id: 1,
  avatar: { gravatar: { hash: '' }, tmdb: {} },
  includeAdult: false,
  sessionId: '13',
  name: 'ludin',
};
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<ShowInfo/>', () => {
  beforeEach(() => {
    store.dispatch(logOut());
  });
  test('Should render the show info', () => {
    renderWithContext(<ShowInfo show={mockTvShows.results[0]} />);
    expect(screen.getByText('House of the Dragon')).toBeInTheDocument();
  });
  test('Should add show to favorites', async () => {
    await store.dispatch(loginSuccess(mockUser));
    renderWithContext(<ShowInfo show={mockTvShows.results[0]} />);
    const button = screen.getByText('Add to Favorites');
    userEvent.click(button);
    jest.spyOn(window, 'alert').mockImplementation();
    await waitFor(() => {
      expect(window.alert).toBeCalledWith('Show Added to favorites');
    });
  });
  test('Should not have a add to favorites button', async () => {
    renderWithContext(<ShowInfo show={mockTvShows.results[0]} />);
    expect(screen.queryByText('Add to Favorites')).not.toBeInTheDocument();
  });
});
