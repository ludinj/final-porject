import React from 'react';
import { ITvShow } from '../../ts/interfaces';
import noImage from '../../images/noImage.png';
import './show-info.scss';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import { addToFavoritesShows } from '../../redux/apiCalls';
import { imageBaseUrl } from '../../utils/constants';
type MovieInfoProps = {
  show: ITvShow | null;
};

const ShowInfo = ({ show }: MovieInfoProps) => {
  const image = `${imageBaseUrl}/t/p/original/${show?.backdropPath}`;
  const overview = show?.overview ? show.overview : 'No description';
  const { currentUser } = useSelector((state: RootState) => state.user);

  const handleAddToFavorites = async () => {
    if (currentUser && show) {
      try {
        await addToFavoritesShows(currentUser, show);
        alert('Show Added to favorites');
      } catch (error) {
        alert(error);
      }
    } else {
      return;
    }
  };

  return (
    <div className='show-info'>
      <div className='show-info__image'>
        <img src={show?.backdropPath ? image : noImage} alt={show?.name} />
      </div>
      <div className='info'>
        <h2>{show?.name}</h2>
        <p>{overview}</p>
        <div className='info-item'>
          <h4>Rating:</h4>
          <p> {show?.voteAverage}</p>
        </div>
        <div className='info-item'>
          <h4>Genres:</h4>
          {show?.genres.map((genre, idx) => {
            return <p key={idx}> {genre.name}.</p>;
          })}
        </div>

        <div className='info-item'>
          <h4> Release date: </h4>
          <p>{show?.firstAirDate}</p>
        </div>
        {currentUser ? (
          <button className='favorite-button' onClick={handleAddToFavorites}>
            Add to Favorites
          </button>
        ) : null}
      </div>
    </div>
  );
};

export default ShowInfo;
