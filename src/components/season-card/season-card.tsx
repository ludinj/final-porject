import React from 'react';
import { ISeason } from '../../ts/interfaces';
import noImage from '../../images/noImage.png';

import './season-card.scss';
import { useNavigate } from 'react-router-dom';
import { EPages } from '../../ts/enums';
import { imageBaseUrl } from '../../utils/constants';

type SeasonCardProps = {
  season: ISeason;
};
const SeasonCard = ({ season }: SeasonCardProps) => {
  const image = `${imageBaseUrl}/t/p/original/${season.posterPath}`;
  const navigate = useNavigate();
  const handleCardClick = () => {
    navigate(`${EPages.season}/${season.seasonNumber}`);
  };

  return (
    <div className='season-card' onClick={handleCardClick}>
      <div className='season__image'>
        <img src={season.posterPath ? image : noImage} alt={season.name} />
        <div className='season-card__info'>
          <p> Season: {season.seasonNumber}</p>
        </div>
      </div>
    </div>
  );
};

export default SeasonCard;
