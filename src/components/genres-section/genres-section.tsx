import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import { useFetch } from '../../hooks/useFetch';
import { EFilters } from '../../ts/enums';
import { IGenre } from '../../ts/interfaces';
import './genres-section.scss';
type GenresSectionProps = {
  media: string;
};

const GenresSection = ({ media }: GenresSectionProps) => {
  const { fetchData } = useFetch();
  const [genres, setGenres] = useState<IGenre[] | undefined>(undefined);
  const [searchParams, setSearchParams] = useSearchParams();
  const genreParam = searchParams.get(EFilters.genre);
  const [activeId, setActiveId] = useState<string | null>(genreParam);

  useEffect(() => {
    const getGenres = async () => {
      const genresResponse = await fetchData(
        `${process.env.REACT_APP_BASE_API_URL}/3/genre/${media}/list`
      );

      setGenres(genresResponse.genres);
    };
    getGenres();
  }, [fetchData, media]);
  const handleGenreClick = (genre: IGenre) => {
    searchParams.set(EFilters.genre, genre.id.toString());
    searchParams.delete(EFilters.page);
    setSearchParams(searchParams);
    setActiveId(genre.id.toString());
  };

  const handleSetAllGenre = () => {
    searchParams.delete(EFilters.page);
    searchParams.delete(EFilters.genre);
    setSearchParams(searchParams);
    setActiveId(null);
  };
  return (
    <div className='genre-section'>
      <div className='genre-item' onClick={handleSetAllGenre}>
        All
      </div>
      {genres
        ? genres.map((genre: IGenre) => {
            return (
              <div
                className={
                  activeId === genre.id.toString()
                    ? 'genre-item active'
                    : 'genre-item'
                }
                key={genre.id}
                onClick={() => handleGenreClick(genre)}
              >
                {genre.name}
              </div>
            );
          })
        : null}
    </div>
  );
};

export default GenresSection;
