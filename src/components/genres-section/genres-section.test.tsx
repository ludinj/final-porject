import GenresSection from './genres-section';

import { render, screen, waitFor } from '@testing-library/react';

import { BrowserRouter, useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

import userEvent from '@testing-library/user-event';

import { ESection } from '../../ts/enums';

export const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element}
        <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}

describe('<GenresSection/>', () => {
  test('Should render the genres', async () => {
    renderWithContext(<GenresSection media={ESection.movie} />);
    await waitFor(() => {
      expect(screen.getByText(/action/i)).toBeInTheDocument();
    });
  });
  test('Should set the genre filters', async () => {
    renderWithContext(<GenresSection media={ESection.movie} />);
    await waitFor(() => {
      expect(screen.getByText(/action/i)).toBeInTheDocument();
    });
    const actionButton = screen.getByText(/action/i);
    userEvent.click(actionButton);

    await waitFor(() => {
      expect(window.location.search).toEqual('?genre=28');
    });
  });
});
