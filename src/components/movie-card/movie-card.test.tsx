import { render, screen, waitFor } from '@testing-library/react';
import MovieCard from './movie-card';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import { mockMovies } from '../../mockData/mockMovies';
import userEvent from '@testing-library/user-event';
import * as router from 'react-router-dom';

export const LocationDisplay = () => {
  const location = router.useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element}
        <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}

describe('<MovieCard/>', () => {
  test('Should render a movie card', () => {
    renderWithContext(<MovieCard movie={mockMovies.results[0]} />);
    expect(screen.getByText('Terrifier 2')).toBeInTheDocument();
  });

  test('Should navigate to movie details on click', async () => {
    renderWithContext(<MovieCard movie={mockMovies.results[0]} />);

    const container = screen.getByTestId(mockMovies.results[0].id);
    userEvent.click(container);

    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        `/movies/${mockMovies.results[0].id}`
      );
    });
  });
});
