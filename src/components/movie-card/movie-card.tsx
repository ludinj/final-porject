import { useNavigate } from 'react-router-dom';
import { EPages } from '../../ts/enums';
import { IMovie } from '../../ts/interfaces';
import noImage from '../../images/noImage.png';
import './movie-card.scss';
import { imageBaseUrl } from '../../utils/constants';

type MovieCardProps = {
  movie: IMovie;
};
const MovieCard = ({ movie }: MovieCardProps) => {
  const navigate = useNavigate();
  const date = movie.releaseDate
    ? new Date(movie.releaseDate).toDateString()
    : '';
  const image = `${imageBaseUrl}/t/p/w500/${movie.posterPath}`;

  const handleClick = () => {
    navigate(`/${EPages.movies}/${movie.id}`);
  };
  return (
    <div className='movie-card' onClick={handleClick} data-testid={movie.id}>
      <img
        src={movie.posterPath ? image : noImage}
        alt={movie.title}
        loading='lazy'
      />
      <div className='movie-card__info'>
        <h4>{movie.title}</h4>
        <p>{date}</p>
        <p>Movie</p>
      </div>
    </div>
  );
};

export default MovieCard;
