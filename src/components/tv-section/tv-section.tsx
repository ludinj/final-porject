import React, { useEffect, useState } from 'react';
import {
  BsFillArrowLeftSquareFill,
  BsFillArrowRightSquareFill,
} from 'react-icons/bs';
import ReactPaginate from 'react-paginate';
import { useSelector } from 'react-redux';
import { useChangeParams } from '../../hooks/useChangeParams';
import { useFetch } from '../../hooks/useFetch';
import { addToFavoritesShows } from '../../redux/apiCalls';
import { RootState } from '../../redux/store';
import { ITvShow } from '../../ts/interfaces';
import { maxPages } from '../../utils/constants';
import { generateYears } from '../../utils/generateYears';
import GenresSection from '../genres-section/genres-section';
import TvCard from '../TvCard/tv-card';
import { AiFillHeart } from 'react-icons/ai';
import './tv-section.scss';
import { useNavigate } from 'react-router-dom';

const TvSection = () => {
  const { loading, fetchData } = useFetch();
  const [tvShows, setTvShows] = useState<ITvShow[] | null>(null);
  const [totalPages, setTotalPages] = useState<number | undefined>(undefined);

  const {
    handlePageClick,
    handleYearChange,
    yearQuery,
    currentPage,
    genreQuery,
    certificationQuery,
  } = useChangeParams();
  const years = generateYears();
  const { currentUser } = useSelector((state: RootState) => state.user);
  const navigate = useNavigate();

  useEffect(() => {
    const getShows = async () => {
      try {
        const TvResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/discover/tv?page=${currentPage}&with_genres=${genreQuery}&first_air_date_year=${yearQuery}&certification_country=US&certification=${certificationQuery}`
        );

        if (!TvResponse) {
          navigate('/*');
          return;
        }
        if (TvResponse.totalPages > maxPages) {
          setTotalPages(maxPages);
        } else {
          setTotalPages(TvResponse.totalPages);
        }
        setTvShows(TvResponse.results);
      } catch (error) {
        alert(error);
      }
    };
    getShows();
  }, [
    fetchData,
    certificationQuery,
    yearQuery,
    genreQuery,
    currentPage,
    navigate,
  ]);

  const handleAddToFavorites = async (
    show: ITvShow,
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    if (currentUser) {
      try {
        await addToFavoritesShows(currentUser, show);
        alert('Show Added to favorites');
      } catch (error) {
        alert(error);
      }
    } else {
      return;
    }
  };

  return (
    <div className='tv-section'>
      <div className='filters'>
        <GenresSection media='tv' />
        <div className='selects'>
          <div className='select-item'>
            <label htmlFor='years'>By Year:</label>
            <select
              name='years'
              id='years'
              onChange={(e) => handleYearChange(e)}
            >
              <option value=''>All</option>
              {years.map((year, idx) => {
                return (
                  <option value={year} key={idx}>
                    {year}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
      </div>

      {loading ? <h3>Loading..</h3> : null}
      {tvShows ? (
        <div className='tv-gird'>
          {tvShows.length > 0 ? (
            tvShows?.map((show: ITvShow, idx) => {
              return (
                <div className='card-container' key={idx}>
                  {currentUser ? (
                    <button
                      onClick={(e) => handleAddToFavorites(show, e)}
                      className='card-container__button'
                    >
                      <AiFillHeart size={20} />
                    </button>
                  ) : null}
                  <TvCard show={show} />
                </div>
              );
            })
          ) : (
            <h3>No Results</h3>
          )}
        </div>
      ) : null}

      {totalPages ? (
        <ReactPaginate
          className='pagination'
          breakLabel='...'
          nextLabel={<BsFillArrowRightSquareFill />}
          onPageChange={(e) => handlePageClick(e)}
          pageRangeDisplayed={5}
          pageCount={totalPages}
          previousLabel={<BsFillArrowLeftSquareFill />}
          pageClassName='page'
          activeClassName='active-page'
          forcePage={parseInt(currentPage) - 1}
          disabledClassName='disableButton'
        />
      ) : null}
    </div>
  );
};

export default TvSection;
