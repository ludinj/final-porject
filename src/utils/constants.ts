export const maxPages = 500;
export const maxCreditsResults = 15;
export const searchDelay = 500;
export const imageBaseUrl = 'https://image.tmdb.org';
export const maxSimilarMovies = 9;
export const maxReviewParagraphs = 3;
export const DEPLOY_URL = 'https://final-porject.vercel.app/';
