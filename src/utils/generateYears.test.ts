import { generateYears } from './generateYears';

describe('generateYears', () => {
  test('Should return and array of numbers from 2022 to 1980', () => {
    const numberArray = generateYears();
    expect(numberArray[0]).toEqual(2022);
    expect(numberArray[numberArray.length - 1]).toEqual(1980);
  });
});
