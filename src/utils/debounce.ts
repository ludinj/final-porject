export const debounce = (cb: Function, delay: number) => {
  let timeOut: NodeJS.Timeout;
  return function (...arg: any[]) {
    if (timeOut) {
      clearTimeout(timeOut);
    }
    timeOut = setTimeout(() => {
      cb(...arg);
    }, delay);
  };
};
