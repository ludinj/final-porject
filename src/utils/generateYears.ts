export const generateYears = () => {
  const years: number[] = [];
  for (let index = 2022; index >= 1980; index--) {
    years.push(index);
  }
  return years;
};
