import React from 'react';
import { useSelector } from 'react-redux';
import { Routes, Route, Navigate } from 'react-router-dom';

import './app.scss';
import MovieSection from './components/movie-section/movie-section';
import TvSection from './components/tv-section/tv-section';
import Layout from './layout/layout';
import Home from './pages/home/home';
import MovieDetails from './pages/movie-details/movie-details';
import Notfound from './pages/not-found/not-found';
import PersonDetails from './pages/person-details/person-details';
import Search from './pages/search/search';
import SeasonDetails from './pages/season-details/season-details';
import TvDetails from './pages/tv-details/tv-details';
import UserDetails from './pages/user-details/user-details';
import { RootState } from './redux/store';
import { EPages } from './ts/enums';

function App() {
  const { currentUser } = useSelector((state: RootState) => state.user);
  return (
    <div className='App'>
      <Routes>
        <Route element={<Layout />}>
          <Route index element={<Home />} />
          <Route path={EPages.home} element={<Home />} />

          <Route path={EPages.movies} element={<MovieSection />} />
          <Route
            path={`${EPages.movies}/:movieId`}
            element={<MovieDetails />}
          />
          <Route path={EPages.tv} element={<TvSection />} />
          <Route path={`${EPages.tv}/:tvId`} element={<TvDetails />} />
          <Route
            path={`${EPages.tv}/:tvId/${EPages.season}/:seasonNumber`}
            element={<SeasonDetails />}
          />
          <Route path={EPages.search} element={<Search />} />
          <Route
            path={`${EPages.person}/:personId`}
            element={<PersonDetails />}
          />
          <Route
            path={EPages.userDetails}
            element={
              currentUser ? (
                <UserDetails />
              ) : (
                <Navigate to={`/${EPages.home}`} />
              )
            }
          />
          <Route path='*' element={<Notfound />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
