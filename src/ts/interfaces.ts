export interface IMovie {
  adult: boolean;
  backdropPath: string;
  genreIds: number[];
  id: number;
  originalLanguage: string;
  originalTitle: string;
  overview: string;
  popularity: number;
  posterPath: string;
  releaseDate: string;
  title: string;
  video: boolean;
  voteAverage: number;
  voteCount: number;
  genres: IGenre[];
}

export interface IGenre {
  id: number;
  name: string;
}
export type PageSelect = { selected: number };

export interface ICertification {
  certification: string;
  meaning: string;
  order: number;
}
export interface ICast {
  adult: boolean;
  gender: number;
  id: number;
  knownForDepartment: string;
  name: string;
  originalName: string;
  popularity: number;
  profilePath: string;
  castId: number;
  character: string;
  creditId: string;
  order: number;
}

export interface ICrew {
  adult: boolean;
  gender: number;
  id: number;
  knownForDepartment: string;
  name: string;
  originalName: string;
  popularity: number;
  profilePath: string;
  creditId: string;
  department: string;
  job: string;
}

export interface ICredits {
  id: number;
  cast: ICast[];
  crew: ICrew[];
}

export interface IAuthorDetails {
  name: string;
  username: string;
  avatarPath: string;
  rating: number;
}

export interface IReview {
  author: string;
  authorDetails: IAuthorDetails;
  content: string;
  createdAt: string;
  id: string;
  updatedAt: string;
  url: string;
}

export interface ITvShow {
  backdropPath: string;
  firstAirDate: string;
  genreIds: number[];
  id: number;
  name: string;
  originCountry: string[];
  originalLanguage: string;
  originalName: string;
  overview: string;
  popularity: number;
  posterPath: string;
  voteAverage: number;
  voteCount: number;
  genres: IGenre[];
  seasons: ISeason[];
}

export interface IPerson {
  adult: boolean;
  alsoKnownAs: string[];
  biography: string;
  birthday: string;
  deathday?: string | null;
  gender: number;
  homepage?: string | null;
  id: number;
  imdbId: string;
  knownForDepartment: string;
  name: string;
  placeOfBirth: string;
  popularity: number;
  profilePath: string;
}

export interface IMedia {
  backdropPath: string;
  firstAirDate: string;
  genreIds: number[];
  id: number;
  title: string;
  originalTitle: string;
  adult: boolean;
  mediaType: string;
  name: string;
  originCountry: string[];
  originalLanguage: string;
  originalName: string;
  overview: string;
  popularity: number;
  posterPath: string;
  voteAverage: number;
  voteCount: number;
  releaseDate: string;
  video: boolean;
  genres: IGenre[];
  seasons: ISeason[];
}

export interface GuestStar {
  character: string;
  creditId: string;
  order: number;
  adult: boolean;
  gender: number;
  id: number;
  knownForDepartment: string;
  name: string;
  originalName: string;
  popularity: number;
  profilePath: string;
}

export interface IEpisode {
  airDate: string;
  episodeNumber: number;
  id: number;
  name: string;
  overview: string;
  productionCode: string;
  runtime: number;
  seasonNumber: number;
  showId: number;
  stillPath: string;
  voteAverage: number;
  voteCount: number;
  crew: ICrew[];
  guestStars: GuestStar[];
}

export interface ISeason {
  id: number;
  episodeCount: number;
  airDate: string;
  episodes: IEpisode[];
  name: string;
  overview: string;
  posterPath: string;
  seasonNumber: number;
}
export interface Gravatar {
  hash: string;
}

export interface ITmdb {
  avatarPath?: string;
}

export interface IAvatar {
  gravatar: Gravatar;
  tmdb: ITmdb;
}

export interface ICurrentUser {
  avatar: IAvatar;
  id: number;
  name: string;
  includeAdult: boolean;
  username: string;
  sessionId: string;
}

export interface IUserState {
  currentUser: ICurrentUser | null;
  isFetching: boolean;
  error: boolean;
}
