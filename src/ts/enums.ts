export enum ESection {
  movie = 'movie',
  tv = 'tv',
}

export enum EFilters {
  year = 'year',
  genre = 'genre',
  certification = 'certification',
  page = 'page',
  query = 'query',
  api_key = 'api_key',
}

export enum EPages {
  home = 'home',
  movies = 'movies',
  tv = 'tv',
  season = 'season',
  person = 'person',
  search = 'search',
  userDetails = 'user-details',
}
