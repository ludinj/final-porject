import { render, screen, waitFor } from '@testing-library/react';
import TvDetails from './tv-details';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<TvDetails/>', () => {
  test('Should render the season  details', async () => {
    renderWithContext(<TvDetails />);
    await waitFor(() => {
      expect(screen.getByText(/Loading/i)).toBeInTheDocument();
    });
  });
});
