import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import MovieCredits from '../../components/movie-credits/movie-credits';
import Reviews from '../../components/reviews/reviews';
import SeasonSection from '../../components/seasons-section/seasons-section';
import ShowInfo from '../../components/show-info/show-info';
import SimilarShows from '../../components/similar-shows/similar-shows';
import { useFetch } from '../../hooks/useFetch';
import { ICredits, IReview, ITvShow } from '../../ts/interfaces';
import { maxSimilarMovies } from '../../utils/constants';
import './tv-details.scss';

const TvDetails = () => {
  const { fetchData, loading } = useFetch();
  const [tvShow, setTvShow] = useState<ITvShow | null>(null);
  const [similarShows, setSimilarShows] = useState<ITvShow[]>([]);
  const [credits, setCredits] = useState<ICredits | null>(null);
  const [reviews, setReviews] = useState<IReview[]>([]);
  const { tvId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    window.scroll({ top: 0 });
    const getShowInfo = async () => {
      try {
        const showResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/tv/${tvId}`
        );
        if (!showResponse) {
          navigate('/*');
          return;
        }
        const creditsResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/tv/${tvId}/credits`
        );
        const reviewsResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/tv/${tvId}/reviews`
        );
        const similarMoviesResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/tv/${tvId}/similar`
        );
        setReviews(reviewsResponse.results);
        setTvShow(showResponse);
        setCredits(creditsResponse);
        setSimilarShows(
          similarMoviesResponse.results.slice(0, maxSimilarMovies)
        );
      } catch (error) {
        alert(error);
      }
    };
    getShowInfo();
  }, [tvId, fetchData, navigate]);

  return (
    <div className='show-details'>
      {!loading ? (
        <div className='show-details__container'>
          <ShowInfo show={tvShow} />
          <SeasonSection seasons={tvShow?.seasons} />
          <SimilarShows shows={similarShows} />
          <Reviews reviews={reviews} />
          <MovieCredits credits={credits} />
        </div>
      ) : (
        <h3>Loading...</h3>
      )}
    </div>
  );
};

export default TvDetails;
