import React from 'react';
import { useNavigate } from 'react-router-dom';
import { EPages } from '../../ts/enums';
import './not-found.scss';
const Notfound = () => {
  const navigate = useNavigate();
  return (
    <div className='not-fount'>
      <h1>Oops!</h1>
      <h2>404- PAGE NOT FOUND.</h2>
      <p>
        The page you are looking for might have been removed had its name
        changed or is temporarily unavailable.
      </p>
      <button onClick={() => navigate(`/${EPages.home}`)}>Go back</button>
    </div>
  );
};

export default Notfound;
