import { render, screen } from '@testing-library/react';
import Notfound from './not-found';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import { ICurrentUser } from '../../ts/interfaces';
import userEvent from '@testing-library/user-event';

export const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element}
        <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}

describe('<Notfound/>', () => {
  test('Should  navigate to home page on click', async () => {
    renderWithContext(<Notfound />);

    const button = screen.getByText(/Go back/i);

    userEvent.click(button);
    expect(screen.getByTestId('location-display')).toHaveTextContent('/home');
  });
});
