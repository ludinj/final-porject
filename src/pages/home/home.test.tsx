import { render, screen, waitFor } from '@testing-library/react';
import Home from './home';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}
describe('<Home/>', () => {
  test('Should render popular movies', async () => {
    renderWithContext(<Home />);

    await waitFor(() => {
      expect(screen.getByText(/Welcome/i)).toBeInTheDocument();
    });
  });
});
