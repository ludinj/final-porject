import React, { useEffect, useState } from 'react';
import { EPages, ESection } from '../../ts/enums';
import bannerImage from '../../images/bannerImage.jpg';
import './home.scss';
import { useFetch } from '../../hooks/useFetch';
import { IMovie, ITvShow } from '../../ts/interfaces';
import MovieCard from '../../components/movie-card/movie-card';
import TvCard from '../../components/TvCard/tv-card';
import { Link } from 'react-router-dom';
const Home = () => {
  const [currentSection, setCurrentSection] = useState<string>(ESection.movie);
  const [popularMovies, setPopularMovies] = useState<IMovie[]>([]);
  const [popularShows, setPopularShows] = useState<ITvShow[]>([]);
  const { fetchData } = useFetch();

  useEffect(() => {
    const getPopularMovies = async () => {
      try {
        const popularMoviesResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/movie/popular`
        );

        setPopularMovies(popularMoviesResponse.results);
      } catch (error) {
        alert(error);
      }
    };
    const getPopularShows = async () => {
      try {
        const popularShowsResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/tv/popular`
        );
        setPopularShows(popularShowsResponse.results);
      } catch (error) {
        alert(error);
      }
    };
    getPopularMovies();
    getPopularShows();
  }, [fetchData]);

  return (
    <div className='home'>
      <div className='home-banner'>
        <h1>Welcome.</h1>
        <p>Millions of movies, TV shows and people to discover.</p>
        <Link to={`/${EPages.movies}`}>Explore now.</Link>
        <div className='banner-image'>
          <img src={bannerImage} alt='' />
        </div>
      </div>

      <div className='popular-section'>
        <div className='popular-navigation'>
          <h2>Popular now.</h2>
          <div
            onClick={() => setCurrentSection(ESection.movie)}
            className={
              currentSection === ESection.movie
                ? 'navigation-div  active'
                : 'navigation-div'
            }
          >
            Movies
          </div>
          <div
            onClick={() => setCurrentSection(ESection.tv)}
            className={
              currentSection === ESection.tv
                ? 'navigation-div  active'
                : 'navigation-div'
            }
          >
            Tv Shows
          </div>
        </div>
        <div className='popular-items__container'>
          {currentSection === ESection.movie
            ? popularMovies.map((movie, idx) => {
                return <MovieCard movie={movie} key={idx} />;
              })
            : null}
          {currentSection === ESection.tv
            ? popularShows.map((show, idx) => {
                return <TvCard show={show} key={idx} />;
              })
            : null}
        </div>
      </div>
    </div>
  );
};

export default Home;
