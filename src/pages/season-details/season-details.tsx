import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useFetch } from '../../hooks/useFetch';
import { ISeason } from '../../ts/interfaces';
import noImage from '../../images/noImage.png';

import './season-details.scss';
import { imageBaseUrl } from '../../utils/constants';

const SeasonDetails = () => {
  const { tvId, seasonNumber } = useParams();
  const [seasonDetails, setSeasonDetails] = useState<ISeason | null>(null);
  const { fetchData } = useFetch();
  const image = `${imageBaseUrl}/t/p/original/${seasonDetails?.posterPath}`;
  const navigate = useNavigate();

  useEffect(() => {
    const getSeasonInfo = async () => {
      try {
        const seasonInfoResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/tv/${tvId}/season/${seasonNumber}`
        );
        if (!seasonInfoResponse) {
          navigate('/*');
          return;
        }

        setSeasonDetails(seasonInfoResponse);
      } catch (error) {
        alert(error);
      }
    };
    getSeasonInfo();
  }, [tvId, seasonNumber, fetchData, navigate]);

  return (
    <div className='season-details'>
      <div className='top-section'>
        <div className='season-image'>
          <img
            src={seasonDetails?.posterPath ? image : noImage}
            alt={seasonDetails?.name}
          />
        </div>
        <div className='season-info'>
          <h2>{seasonDetails?.name}</h2>
          <p>
            {seasonDetails?.overview
              ? seasonDetails?.overview
              : 'No description'}
          </p>
          <div className='details-item'>
            <h4>Air Date:</h4>
            <p>{seasonDetails?.airDate}</p>
          </div>
        </div>
      </div>
      <div className='season-episodes'>
        <h1>Episodes.</h1>
        {seasonDetails?.episodes.map((episode, idx) => {
          return (
            <div className='season-episode' key={idx}>
              <h4>Episode {episode.episodeNumber}</h4>
              <p>{episode.overview ? episode.overview : 'No description'}</p>
              <p>{episode.airDate}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SeasonDetails;
