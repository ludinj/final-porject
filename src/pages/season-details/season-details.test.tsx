import { render, screen, waitFor } from '@testing-library/react';
import SeasonDetails from './season-details';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<SeasonDetails/>', () => {
  test('Should render the season  details', async () => {
    renderWithContext(<SeasonDetails />);
    await waitFor(() => {
      expect(
        screen.getByText(/Dr. Gregory House is devoid of bedside/i)
      ).toBeInTheDocument();
    });
  });
});
