import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import FavoriteMovies from '../../components/favorite-movies/favorite-movies';
import FavoriteShows from '../../components/favorite-shows/favorite-shows';
import { RootState } from '../../redux/store';
import { ESection } from '../../ts/enums';
import { imageBaseUrl } from '../../utils/constants';
import noImage from '../../images/noImage.png';

import './user-details.scss';
const UserDetails = () => {
  const [currentSection, setCurrentSection] = useState<string>(ESection.movie);
  const { currentUser } = useSelector((state: RootState) => state.user);
  const avatar = `${imageBaseUrl}/t/p/original/${currentUser?.avatar.tmdb.avatarPath}`;
  return (
    <div className='user-details'>
      <div className='user-info'>
        <div className='info-item'>
          <h4>Avatar:</h4>
          <img
            src={currentUser?.avatar.tmdb.avatarPath ? avatar : noImage}
            alt={currentUser?.name}
          />
        </div>
        <div className='info-item'>
          <h4>Username:</h4>
          <p>{currentUser?.username}</p>
        </div>
        <div className='info-item'>
          <h4>Name:</h4>
          <p>{currentUser?.name}</p>
        </div>
      </div>
      <h1>Your Favorites.</h1>

      <div className='user-content'>
        <div className='selection'>
          <div
            className={
              currentSection === ESection.movie
                ? 'selection-item active'
                : 'selection-item '
            }
            onClick={() => setCurrentSection(ESection.movie)}
          >
            <h3>Movies</h3>
          </div>
          <div
            className={
              currentSection === ESection.tv
                ? 'selection-item active'
                : 'selection-item '
            }
            onClick={() => setCurrentSection(ESection.tv)}
          >
            <h3>Shows</h3>
          </div>
        </div>
        {currentSection === ESection.movie ? (
          <FavoriteMovies />
        ) : (
          <FavoriteShows />
        )}
      </div>
    </div>
  );
};

export default UserDetails;
