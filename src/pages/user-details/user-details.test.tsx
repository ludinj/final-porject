import { render } from '@testing-library/react';
import UserDetails from './user-details';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}
describe('<UserDetails/>', () => {
  test('Should render the user details details', async () => {
    renderWithContext(<UserDetails />);
  });
});
