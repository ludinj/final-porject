import { render, screen, waitFor } from '@testing-library/react';
import PersonDetails from './person-details';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<PersonDetails/>', () => {
  test('Should render the person details', async () => {
    renderWithContext(<PersonDetails />);

    await waitFor(() => {
      expect(
        screen.getByText(
          /An American and Canadian actor, producer and semi-retired professional wrestler/i
        )
      ).toBeInTheDocument();
    });
  });
});
