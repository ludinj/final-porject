import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useFetch } from '../../hooks/useFetch';
import { IPerson } from '../../ts/interfaces';
import noImage from '../../images/noImage.png';

import './person-details.scss';
import { imageBaseUrl } from '../../utils/constants';
const PersonDetails = () => {
  const { personId } = useParams();
  const { fetchData } = useFetch();
  const [person, setPerson] = useState<IPerson | null>(null);
  const image = `${imageBaseUrl}/t/p/original/${person?.profilePath}`;
  const navigate = useNavigate();

  useEffect(() => {
    const getPersonInfo = async () => {
      try {
        const personResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/person/${personId}`
        );
        if (!personResponse) {
          navigate('/*');
          return;
        }

        setPerson(personResponse);
      } catch (error) {
        alert(error);
      }
    };
    getPersonInfo();
  }, [fetchData, personId, navigate]);

  return (
    <div className='person-details'>
      <div className='top'>
        <div className='person-image'>
          <img src={person?.profilePath ? image : noImage} alt={person?.name} />
        </div>

        <div className='person-details__info'>
          <h2>{person?.name}.</h2>
          <div className='info-item'>
            <h4>Gender: </h4>
            {person?.gender ? (
              <p>{person?.gender === 1 ? 'Female' : 'Male'}</p>
            ) : (
              <p>No information</p>
            )}
          </div>
          <div className='info-item'>
            <h4>Birthday: </h4>
            {person?.birthday ? (
              <p>{person?.birthday}</p>
            ) : (
              <p>No information</p>
            )}
          </div>
          <div className='info-item'>
            <h4>Deathday: </h4>
            <p>{person?.deathday ? person?.deathday : 'Alive.'}</p>
          </div>
        </div>
      </div>
      <div className='biography'>
        <h1>Biography.</h1>
        {person?.biography ? (
          <p>{person?.biography}</p>
        ) : (
          <h3>No information</h3>
        )}
      </div>
    </div>
  );
};

export default PersonDetails;
