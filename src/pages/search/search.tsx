import React, { useEffect, useState } from 'react';
import {
  BsFillArrowLeftSquareFill,
  BsFillArrowRightSquareFill,
} from 'react-icons/bs';
import ReactPaginate from 'react-paginate';
import MediaCard from '../../components/media-card/media-card';
import { useChangeParams } from '../../hooks/useChangeParams';
import { useFetch } from '../../hooks/useFetch';
import { IMedia } from '../../ts/interfaces';
import { maxPages, searchDelay } from '../../utils/constants';
import { debounce } from '../../utils/debounce';
import './search.scss';
const Search = () => {
  const { fetchData, loading } = useFetch();
  const [searchItems, setSearchItems] = useState<IMedia[] | null>(null);
  const [totalPages, setTotalPages] = useState<number | undefined>(undefined);

  const { handlePageClick, currentPage, query, handleSearch } =
    useChangeParams();

  useEffect(() => {
    if (!query) {
      setSearchItems(null);
      setTotalPages(undefined);
      return;
    }
    const getItems = async () => {
      if (!query) {
        return;
      }
      const searchResponse = await fetchData(
        `${process.env.REACT_APP_BASE_API_URL}/3/search/multi?page=${currentPage}&query=${query}`
      );

      if (searchResponse.totalPages > maxPages) {
        setTotalPages(maxPages);
      } else {
        setTotalPages(searchResponse.totalPages);
      }
      setSearchItems(searchResponse.results);
    };
    getItems();
  }, [fetchData, query, currentPage]);

  const debounceSearch = debounce(handleSearch, searchDelay);
  return (
    <div className='search'>
      <h1>Search your favorite movie or tv show.</h1>
      <input type='text' placeholder='Search...' onChange={debounceSearch} />

      {searchItems ? (
        <>
          <div className='search-gird'>
            {searchItems?.length > 0 ? (
              searchItems.map((media: IMedia) => {
                return <MediaCard media={media} key={media.id} />;
              })
            ) : (
              <h3>No Results</h3>
            )}
          </div>

          {totalPages ? (
            <ReactPaginate
              className='pagination'
              breakLabel='...'
              nextLabel={<BsFillArrowRightSquareFill />}
              onPageChange={(e) => handlePageClick(e)}
              pageRangeDisplayed={5}
              pageCount={totalPages}
              previousLabel={<BsFillArrowLeftSquareFill />}
              pageClassName='page'
              activeClassName='active-page'
              forcePage={parseInt(currentPage) - 1}
              disabledClassName='disableButton'
            />
          ) : null}
        </>
      ) : null}
    </div>
  );
};

export default Search;
