import { render, screen, waitFor } from '@testing-library/react';
import Search from './search';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import userEvent from '@testing-library/user-event';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<Search/>', () => {
  test('Should render a list of movies with the query', async () => {
    renderWithContext(<Search />);
    const searchInput = screen.getByPlaceholderText('Search...');
    userEvent.type(searchInput, 'batman');
    await waitFor(() => {
      expect(
        screen.getByText(/Batman and Superman: Battle of the Super Sons/i)
      ).toBeInTheDocument();
    });
  });
  test('Should render no results', async () => {
    renderWithContext(<Search />);
    const searchInput = screen.getByPlaceholderText('Search...');
    userEvent.type(searchInput, 'random words');
    await waitFor(() => {
      expect(screen.getByText(/no results/i)).toBeInTheDocument();
    });
  });
});
