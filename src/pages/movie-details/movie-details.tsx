import { useEffect, useState } from 'react';
import './movie-details.scss';
import { useNavigate, useParams } from 'react-router-dom';
import { useFetch } from '../../hooks/useFetch';
import { ICredits, IMovie, IReview } from '../../ts/interfaces';
import MovieInfo from '../../components/movie-info/movie-info';
import MovieCredits from '../../components/movie-credits/movie-credits';
import SimilarMovies from '../../components/similar-movies/similar-movies';
import Reviews from '../../components/reviews/reviews';
import { maxSimilarMovies } from '../../utils/constants';

const MovieDetails = () => {
  const { fetchData, loading } = useFetch();
  const [movie, setMovie] = useState<IMovie | null>(null);
  const [similarMovies, setSimilarMovies] = useState<IMovie[]>([]);
  const [credits, setCredits] = useState<ICredits | null>(null);
  const [reviews, setReviews] = useState<IReview[]>([]);
  const { movieId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const getMovieInfo = async () => {
      try {
        const movieResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/movie/${movieId}`
        );
        if (!movieResponse) {
          navigate('/*');
          return;
        }
        const creditsResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/movie/${movieId}/credits`
        );
        const reviewsResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/movie/${movieId}/reviews`
        );

        const similarMoviesResponse = await fetchData(
          `${process.env.REACT_APP_BASE_API_URL}/3/movie/${movieId}/similar`
        );
        setMovie(movieResponse);
        setReviews(reviewsResponse.results);
        setCredits(creditsResponse);
        setSimilarMovies(
          similarMoviesResponse.results.slice(0, maxSimilarMovies)
        );
      } catch (error) {
        alert(error);
      }
    };
    getMovieInfo();
  }, [movieId, fetchData, navigate]);

  return (
    <div className='movie-details'>
      {!loading ? (
        <div className='movie-details__container'>
          <MovieInfo movie={movie} />
          <SimilarMovies movies={similarMovies} />
          <Reviews reviews={reviews} />
          <MovieCredits credits={credits} />
        </div>
      ) : (
        <h2>Loading...</h2>
      )}
    </div>
  );
};

export default MovieDetails;
