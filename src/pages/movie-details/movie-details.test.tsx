import { render, screen, waitFor } from '@testing-library/react';
import MovieDetails from './movie-details';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<MovieDetails/>', () => {
  test('Should render the movie details', async () => {
    renderWithContext(<MovieDetails />);

    await waitFor(() => {
      expect(screen.getByText(/Loading/i)).toBeInTheDocument();
    });
  });
});
