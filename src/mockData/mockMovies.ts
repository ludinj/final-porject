import { IMovie } from '../ts/interfaces';

export const mockMovies = {
  results: [
    {
      adult: false,
      backdropPath: '/y5Z0WesTjvn59jP6yo459eUsbli.jpg',
      genreIds: [27, 53],
      id: 663712,
      originalLanguage: 'en',
      originalTitle: 'Terrifier 2',
      overview:
        "After being resurrected by a sinister entity, Art the Clown returns to Miles County where he must hunt down and destroy a teenage girl and her younger brother on Halloween night.  As the body count rises, the siblings fight to stay alive while uncovering the true nature of Art's evil intent.",
      popularity: 7842.699,
      posterPath: '/b6IRp6Pl2Fsq37r9jFhGoLtaqHm.jpg',
      releaseDate: '2022-10-06',
      title: 'Terrifier 2',
      video: false,
      voteAverage: 7.1,
      voteCount: 341,
      genres: [{ id: 12345, name: 'R' }],
    },
    {
      adult: false,
      backdropPath: '/bQXAqRx2Fgc46uCVWgoPz5L5Dtr.jpg',
      genreIds: [28, 14, 878],
      id: 436270,
      originalLanguage: 'en',
      originalTitle: 'Black Adam',
      overview:
        'Nearly 5,000 years after he was bestowed with the almighty powers of the Egyptian gods—and imprisoned just as quickly—Black Adam is freed from his earthly tomb, ready to unleash his unique form of justice on the modern world.',
      popularity: 4085.77,
      posterPath: '/3zXceNTtyj5FLjwQXuPvLYK5YYL.jpg',
      releaseDate: '2022-10-19',
      title: 'Black Adam',
      video: false,
      voteAverage: 7,
      voteCount: 784,
      genres: [{ id: 12345, name: 'R' }],
    },
    {
      adult: false,
      backdropPath: '/askg3SMvhqEl4OL52YuvdtY40Yb.jpg',
      genreIds: [10751, 16, 14, 10402, 35, 12],
      id: 354912,
      originalLanguage: 'en',
      originalTitle: 'Coco',
      overview:
        "Despite his family’s baffling generations-old ban on music, Miguel dreams of becoming an accomplished musician like his idol, Ernesto de la Cruz. Desperate to prove his talent, Miguel finds himself in the stunning and colorful Land of the Dead following a mysterious chain of events. Along the way, he meets charming trickster Hector, and together, they set off on an extraordinary journey to unlock the real story behind Miguel's family history.",
      popularity: 3795.511,
      posterPath: '/gGEsBPAijhVUFoiNpgZXqRVWJt2.jpg',
      releaseDate: '2017-10-27',
      title: 'Coco',
      video: false,
      voteAverage: 8.2,
      voteCount: 16392,
      genres: [{ id: 12345, name: 'R' }],
    },
    {
      adult: false,
      backdropPath: '/naNXYdBzTEb1KwOdi1RbBkM9Zv1.jpg',
      genreIds: [27, 53],
      id: 420634,
      originalLanguage: 'en',
      originalTitle: 'Terrifier',
      overview:
        'On Halloween night, a young woman finds herself as the obsession of a sadistic murderer known as Art the Clown.',
      popularity: 2469.321,
      posterPath: '/6PQqC4SbY910VvyVad6mvsboILU.jpg',
      releaseDate: '2016-10-15',
      title: 'Terrifier',
      video: false,
      voteAverage: 6.5,
      voteCount: 816,
      genres: [{ id: 12345, name: 'R' }],
    },
    {
      adult: false,
      backdropPath: '/pGx6O6IwqADOsgmqWzPysmWnOyr.jpg',
      genreIds: [28, 14],
      id: 732459,
      originalLanguage: 'en',
      originalTitle: 'Blade of the 47 Ronin',
      overview:
        'In this sequel to "47 Ronin," a new class of warriors emerges among the Samurai clans to keep a sought-after sword from falling into the wrong hands.',
      popularity: 2396.57,
      posterPath: '/kjFDIlUCJkcpFxYKtE6OsGcAfQQ.jpg',
      releaseDate: '2022-10-25',
      title: 'Blade of the 47 Ronin',
      video: false,
      voteAverage: 7.2,
      voteCount: 33,
      genres: [{ id: 12345, name: 'R' }],
    },
    {
      adult: false,
      backdropPath: '/tIX6j3NzadlwGcJ52nuWdmtOQkg.jpg',
      genreIds: [27, 53, 9648],
      id: 717728,
      originalLanguage: 'en',
      originalTitle: 'Jeepers Creepers: Reborn',
      overview:
        'Forced to travel with her boyfriend to a horror festival, Laine begins to experience disturbing visions associated with the urban legend of The Creeper. As the festival arrives and the blood-soaked entertainment builds to a frenzy, she becomes the center of it while something unearthly has been summoned.',
      popularity: 2156.734,
      posterPath: '/aGBuiirBIQ7o64FmJxO53eYDuro.jpg',
      releaseDate: '2022-09-15',
      title: 'Jeepers Creepers: Reborn',
      video: false,
      voteAverage: 5.7,
      voteCount: 409,
      genres: [{ id: 12345, name: 'R' }],
    },
    {
      adult: false,
      backdropPath: '/iS9U3VHpPEjTWnwmW56CrBlpgLj.jpg',
      genreIds: [14, 35, 10751],
      id: 642885,
      originalLanguage: 'en',
      originalTitle: 'Hocus Pocus 2',
      overview:
        "29 years since the Black Flame Candle was last lit, the 17th-century Sanderson sisters are resurrected, and they are looking for revenge. Now it's up to three high school students to stop the ravenous witches from wreaking a new kind of havoc on Salem before dawn on All Hallow's Eve.",
      popularity: 1966.006,
      posterPath: '/7ze7YNmUaX81ufctGqt0AgHxRtL.jpg',
      releaseDate: '2022-09-27',
      title: 'Hocus Pocus 2',
      video: false,
      voteAverage: 7.6,
      voteCount: 999,
      genres: [{ id: 12345, name: 'R' }],
    },
    {
      adult: false,
      backdropPath: '/1DBDwevWS8OhiT3wqqlW7KGPd6m.jpg',
      genreIds: [53],
      id: 985939,
      originalLanguage: 'en',
      originalTitle: 'Fall',
      overview:
        'For best friends Becky and Hunter, life is all about conquering fears and pushing limits. But after they climb 2,000 feet to the top of a remote, abandoned radio tower, they find themselves stranded with no way down. Now Becky and Hunter’s expert climbing skills will be put to the ultimate test as they desperately fight to survive the elements, a lack of supplies, and vertigo-inducing heights',
      popularity: 1888.082,
      posterPath: '/spCAxD99U1A6jsiePFoqdEcY0dG.jpg',
      releaseDate: '2022-08-11',
      title: 'Fall',
      video: false,
      voteAverage: 7.3,
      voteCount: 1616,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/etP5jwlwvkNhwe7jnI2AyA6ZKrR.jpg',
      genreIds: [878],
      id: 575322,
      originalLanguage: 'en',
      originalTitle: 'Звёздный разум',
      overview:
        "After depleting Earth's resources for centuries, humankind's survival requires an exodus to outer space. An international expedition is quickly formed to find a suitable new planet, but when plans go awry, the crew is suddenly stranded without power on a strange planet, where something unimaginable lies in wait.",
      popularity: 1846.127,
      posterPath: '/rFljUdOozFEv6HDHIFpFvcYW0ec.jpg',
      releaseDate: '2022-01-06',
      title: 'Project Gemini',
      video: false,
      voteAverage: 5.6,
      voteCount: 147,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/5GA3vV1aWWHTSDO5eno8V5zDo8r.jpg',
      genreIds: [27, 53],
      id: 760161,
      originalLanguage: 'en',
      originalTitle: 'Orphan: First Kill',
      overview:
        'After escaping from an Estonian psychiatric facility, Leena Klammer travels to America by impersonating Esther, the missing daughter of a wealthy family. But when her mask starts to slip, she is put against a mother who will protect her family from the murderous “child” at any cost.',
      popularity: 1829.276,
      posterPath: '/pHkKbIRoCe7zIFvqan9LFSaQAde.jpg',
      releaseDate: '2022-07-27',
      title: 'Orphan: First Kill',
      video: false,
      voteAverage: 6.8,
      voteCount: 1208,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/aTovumsNlDjof7YVoU5nW2RHaYn.jpg',
      genreIds: [27, 53],
      id: 616820,
      originalLanguage: 'en',
      originalTitle: 'Halloween Ends',
      overview:
        'Four years after the events of Halloween in 2018, Laurie has decided to liberate herself from fear and rage and embrace life. But when a young man is accused of killing a boy he was babysitting, it ignites a cascade of violence and terror that will force Laurie to finally confront the evil she can’t control, once and for all.',
      popularity: 1785.265,
      posterPath: '/h1FGQ6FRW6kNx4ACxjCJ18ssW3Y.jpg',
      releaseDate: '2022-10-12',
      title: 'Halloween Ends',
      video: false,
      voteAverage: 6.6,
      voteCount: 820,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/bJa3RcFKgtVKJqTJCSSuBQeP9c8.jpg',
      genreIds: [27],
      id: 86328,
      originalLanguage: 'en',
      originalTitle: 'Terrifier',
      overview:
        'After witnessing a brutal murder on Halloween night, a young woman becomes the next target of a maniacal entity.',
      popularity: 1747.118,
      posterPath: '/gb6rq2nD0jRrN0dCzigg2MxXNsB.jpg',
      releaseDate: '2011-08-09',
      title: 'Terrifier',
      video: false,
      voteAverage: 6.8,
      voteCount: 37,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/5hoS3nEkGGXUfmnu39yw1k52JX5.jpg',
      genreIds: [28, 12, 14],
      id: 960704,
      originalLanguage: 'ja',
      originalTitle: '鋼の錬金術師 完結編 最後の錬成',
      overview:
        'The Elric brothers’ long and winding journey comes to a close in this epic finale, where they must face off against an unworldly, nationwide threat.',
      popularity: 1733.109,
      posterPath: '/AeyiuQUUs78bPkz18FY3AzNFF8b.jpg',
      releaseDate: '2022-06-24',
      title: 'Fullmetal Alchemist: The Final Alchemy',
      video: false,
      voteAverage: 6.2,
      voteCount: 110,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/zt6sKnx9dEiRCb7oVMlfmmMGJMO.jpg',
      genreIds: [28, 35, 53],
      id: 718930,
      originalLanguage: 'en',
      originalTitle: 'Bullet Train',
      overview:
        "Unlucky assassin Ladybug is determined to do his job peacefully after one too many gigs gone off the rails. Fate, however, may have other plans, as Ladybug's latest mission puts him on a collision course with lethal adversaries from around the globe—all with connected, yet conflicting, objectives—on the world's fastest train.",
      popularity: 1553.297,
      posterPath: '/tVxDe01Zy3kZqaZRNiXFGDICdZk.jpg',
      releaseDate: '2022-07-03',
      title: 'Bullet Train',
      video: false,
      voteAverage: 7.5,
      voteCount: 2142,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/nnUQqlVZeEGuCRx8SaoCU4XVHJN.jpg',
      genreIds: [14, 12, 10751],
      id: 532639,
      originalLanguage: 'en',
      originalTitle: 'Pinocchio',
      overview:
        'A wooden puppet embarks on a thrilling adventure to become a real boy.',
      popularity: 1495.034,
      posterPath: '/g8sclIV4gj1TZqUpnL82hKOTK3B.jpg',
      releaseDate: '2022-09-07',
      title: 'Pinocchio',
      video: false,
      voteAverage: 6.7,
      voteCount: 1005,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/tSxbUnrnWlR5dQvUgqMI7sACmFD.jpg',
      genreIds: [14, 28, 18],
      id: 779782,
      originalLanguage: 'en',
      originalTitle: 'The School for Good and Evil',
      overview:
        'Best friends Sophie and Agatha navigate an enchanted school for young heroes and villains — and find themselves on opposing sides of the battle between good and evil.',
      popularity: 1452.606,
      posterPath: '/6oZeEu1GDILdwezmZ5e2xWISf1C.jpg',
      releaseDate: '2022-10-19',
      title: 'The School for Good and Evil',
      video: false,
      voteAverage: 7.3,
      voteCount: 447,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/2iGUavwv86Hubv3V5Iq4qEQXDfE.jpg',
      genreIds: [18, 53, 27],
      id: 848058,
      originalLanguage: 'es',
      originalTitle: 'Cerdita',
      overview:
        'A bullied overweight teenager sees a glimpse of hope when her tormentors are brutally abducted by a mesmerizing stranger.',
      popularity: 1443.877,
      posterPath: '/8EIQAvJjXdbNDMmBtHtgGqbc09V.jpg',
      releaseDate: '2022-10-07',
      title: 'Piggy',
      video: false,
      voteAverage: 6.8,
      voteCount: 152,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/ttkibtcAjoilW1PbTIFy9U9YOdB.jpg',
      genreIds: [53, 28],
      id: 916719,
      originalLanguage: 'en',
      originalTitle: 'Code Name Banshee',
      overview:
        "Caleb, a former government assassin in hiding, who resurfaces when his protégé, the equally deadly killer known as Banshee, discovers a bounty has been placed on Caleb's head.",
      popularity: 1435.273,
      posterPath: '/g29dShv0wHJUvif2KPq039imds5.jpg',
      releaseDate: '2022-07-01',
      title: 'Code Name Banshee',
      video: false,
      voteAverage: 4.8,
      voteCount: 40,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/60UN7vvcWWggLe0Uz9EFZJx718P.jpg',
      genreIds: [53],
      id: 879538,
      originalLanguage: 'en',
      originalTitle: 'Crawlspace',
      overview:
        'After witnessing a brutal murder in a cabin, a man hides in a crawlspace while the killers scour the property for a hidden fortune. As they draw nearer, he must decide if the crawlspace will be his tomb or the battleground in his fight for survival.',
      popularity: 1357.829,
      posterPath: '/qEu6qI5sVoIe10gD1BQBqxcNIW2.jpg',
      releaseDate: '2022-03-31',
      title: 'Crawlspace',
      video: false,
      voteAverage: 7.1,
      voteCount: 61,
      genres: [{ id: 28, name: 'Action' }],
    },
    {
      adult: false,
      backdropPath: '/aIkG2V4UXrfkxMdJZmq30xO0QQr.jpg',
      genreIds: [878, 12, 28],
      id: 791155,
      originalLanguage: 'en',
      originalTitle: 'Secret Headquarters',
      overview:
        'While hanging out after school, Charlie and his friends discover the headquarters of the world’s most powerful superhero hidden beneath his home. When villains attack, they must team up to defend the headquarters and save the world.',
      popularity: 1272.407,
      posterPath: '/8PsHogUfvjWPGdWAI5uslDhHDx7.jpg',
      releaseDate: '2022-08-12',
      title: 'Secret Headquarters',
      video: false,
      voteAverage: 6.9,
      voteCount: 142,
      genres: [{ id: 28, name: 'Action' }],
    },
  ],
};
