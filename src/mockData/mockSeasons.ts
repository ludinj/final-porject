import { ISeason } from '../ts/interfaces';

export const mockSeasons: ISeason[] = [
  {
    airDate: '2010-10-11',
    episodeCount: 54,
    episodes: [],
    id: 3646,
    name: 'Specials',
    overview: '',
    posterPath: '/rliqimvTCs9qWmIsMIyhwCVohyg.jpg',
    seasonNumber: 0,
  },
  {
    airDate: '2010-10-31',
    episodeCount: 6,
    id: 3643,
    episodes: [],

    name: 'Season 1',
    overview:
      'Rick Grimes wakes up from a coma to a world overrun by zombies, on a journey to find his family he must learn to survive the streets of post-apocalyptic Atlanta.',
    posterPath: '/yaOB2Y8GcoXwjNQ3apq67bMbNHF.jpg',
    seasonNumber: 1,
  },
  {
    airDate: '2011-10-16',
    episodeCount: 13,
    id: 3644,
    name: 'Season 2',
    episodes: [],

    overview:
      "Under Rick's leadership, the group leave Atlanta in search of sanctuary.",
    posterPath: '/wpG9SDyz23t3vU8dTbtSvEkxv8r.jpg',
    seasonNumber: 2,
  },
  {
    airDate: '2012-10-14',
    episodeCount: 16,
    episodes: [],

    id: 3645,
    name: 'Season 3',
    overview:
      'Having seemingly found a place of security, the group are faced with an unprecedented new threat.',
    posterPath: '/mDVPeQ5ZaaeO2Qh7VlXZchHHuLN.jpg',
    seasonNumber: 3,
  },
  {
    airDate: '2013-10-13',
    episodeCount: 16,
    id: 3647,
    name: 'Season 4',
    episodes: [],

    overview:
      'As the group settle into life in a stable shelter, a new danger threatens disaster.',
    posterPath: '/pLXlKKQOoUZAjWsXwacZCj6SWIb.jpg',
    seasonNumber: 4,
  },
  {
    airDate: '2014-10-12',
    episodeCount: 16,
    episodes: [],

    id: 60391,
    name: 'Season 5',
    overview:
      'After the season 4 finale left most of the main characters at the mercy of the sadistic inhabitants of Terminus. Season 5 will offer new directions for the group of survivors as scientist Eugene Porter promises a cure to the zombie virus if he can be safely escorted to Washington DC, but getting there is easier said than done.',
    posterPath: '/AjIoXsAD3HgyV9CITIBDgQ0079c.jpg',
    seasonNumber: 5,
  },
  {
    airDate: '2015-10-11',
    episodeCount: 16,
    episodes: [],
    id: 68814,
    name: 'Season 6',
    overview:
      'Rick attempts to use his authority in Alexandria to keep the inhabitants safe, even as a new threat looms.',
    posterPath: '/oTx4EqiJPLYT0Un6kWzvNMQrOLb.jpg',
    seasonNumber: 6,
  },
  {
    airDate: '2016-10-23',
    episodeCount: 16,
    episodes: [],
    id: 76834,
    name: 'Season 7',
    overview:
      "Rick and his group's world becomes even more brutal due to Negan's deadly example of what happens if they don't live under his rules. Everyone must begin again.",
    posterPath: '/xb5npbwmXTod7OBd7GMhenR0QO5.jpg',
    seasonNumber: 7,
  },
  {
    airDate: '2017-10-22',
    episodeCount: 16,
    episodes: [],
    id: 91735,
    name: 'Season 8',
    overview:
      'Rick and his survivors bring "All-Out War" to Negan and his forces. The Saviors are larger, better-equipped, and ruthless - but Rick and the unified communities are fighting for the promise of a brighter future. The battle lines are drawn as they launch into a kinetic, action-packed offensive.',
    posterPath: '/wmv0oIun52Xeq65sBKfHiUkiBKc.jpg',
    seasonNumber: 8,
  },
  {
    airDate: '2018-10-07',
    episodeCount: 16,
    episodes: [],
    id: 109531,
    name: 'Season 9',
    overview:
      'With the defeat of Negan and the Saviors, the survivors are now rebuilding civilisation under Rick’s leadership. However, the group are forced to face their biggest threat yet as the walkers around them have started whispering.',
    posterPath: '/5KKovNGmh20sDBnyFyuaYTOSmh8.jpg',
    seasonNumber: 9,
  },
  {
    airDate: '2019-10-06',
    episodeCount: 22,
    episodes: [],
    id: 123967,
    name: 'Season 10',
    overview:
      'It is now Spring, a few months after the end of Season 9, when our group of survivors dared to cross into Whisperer territory during the harsh winter. The collected communities are still dealing with the after effects of Alpha’s horrific display of power, reluctantly respecting the new borderlines being imposed on them, all while organising themselves into a militia-style fighting force, preparing for a battle that may be unavoidable. But the Whisperers are a threat unlike any they have ever faced. Backed by a massive horde of the dead it is seemingly a fight they cannot win.',
    posterPath: '/4q6BVaoyA8qaq5Se59QL1cxUc8i.jpg',
    seasonNumber: 10,
  },
  {
    airDate: '2021-08-22',
    episodeCount: 24,
    episodes: [],
    id: 189337,
    name: 'Season 11',
    overview:
      'All who live in Alexandria struggle to refortify it and feed its increasing number of residents, which include the survivors from the fall of the Kingdom and the burning of Hilltop; along with Maggie and her new group, the Wardens. Alexandria has more people than it can manage to feed and protect. Their situation is dire as tensions heat up over past events and self-preservation rises to the surface within the ravaged walls. They must secure more food while they attempt to restore Alexandria before it collapses like countless other communities they have come across throughout the years. But where and how? More haggard and hungrier than ever before, they must dig deeper to find the effort and strength to safeguard the lives of their children, even if it means losing their own. Meanwhile, unbeknownst to those at Alexandria, Eugene, Ezekiel, Yumiko, and Princess are still being held captive by mysterious soldiers who are members of a larger and unforthcoming group.',
    posterPath: '/xf9wuDcqlUPWABZNeDKPbZUjWx0.jpg',
    seasonNumber: 11,
  },
];
