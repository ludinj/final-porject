export const mockUser = {
  avatar: {
    gravatar: {
      hash: '90c2c21f01734e60e8b674860bb84215',
    },
    tmdb: {
      avatarPath: '/fymqt8d53w2Sm9LyaAWV0zzTiwy.jpg',
    },
  },
  id: 11470234,
  iso6391: 'es',
  iso31661: 'VE',
  name: 'Ludin jaimes',
  includeAdult: false,
  username: 'ludinj',
  sessionId: '',
};
