import { rest } from 'msw';
import { mockDetailsSeason } from './mockDetailSeason';
import { mockFavoriteMovies } from './mockFavoriteMovies';
import { mockFavoriteShows } from './mockFavoriteShows';
import { mockGenres } from './mockGenres';

import { mockMovies } from './mockMovies';
import { mockMultiSearchResponse } from './mockMultiSearchResponse';
import { mockPerson } from './mockPerson';
import { mockReviews } from './mockReviews';
import { mockTvShows } from './mockTvShows';

export const handlers = [
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/discover/movie/*`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockMovies));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/movie/*`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockMovies.results[0]));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/movie/*/reviews`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockReviews));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/genre/movie/list`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockGenres));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/search/multi`,
    (req, res, ctx) => {
      const pageParams = new URLSearchParams(window.location.search);
      const query = pageParams.get('query');
      if (query === 'batman') {
        return res(ctx.status(200), ctx.json(mockMultiSearchResponse));
      } else {
        return res(ctx.status(200), ctx.json({ results: [] }));
      }
    }
  ),
  rest.post(
    `${process.env.REACT_APP_BASE_API_URL}/3/account/*/favorite*`,
    (req, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json({
          success: true,
          status_code: 1,
          status_message: 'Success.',
        })
      );
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/account/*/favorite/tv`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockFavoriteShows));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/account/*/favorite/movies`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockFavoriteMovies));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/person/*`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockPerson));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/tv/popular*`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockTvShows));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/movie/popular*`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockMovies));
    }
  ),
  rest.get(
    `${process.env.REACT_APP_BASE_API_URL}/3/tv/*/season/*`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockDetailsSeason));
    }
  ),
  rest.get(`${process.env.REACT_APP_BASE_API_URL}/3/tv/*`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockTvShows.results[0]));
  }),
];
