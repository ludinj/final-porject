export const mockFavoriteShows = {
  page: 1,
  results: [
    {
      adult: false,
      backdropPath: '/zaulpwl355dlKkvtAiSBE5LaoWA.jpg',
      genreIds: [10759, 18, 10765],
      id: 1402,
      originCountry: ['US'],
      originalLanguage: 'en',
      originalName: 'The Walking Dead',
      overview:
        "Sheriff's deputy Rick Grimes awakens from a coma to find a post-apocalyptic world dominated by flesh-eating zombies. He sets out to find his family and encounters many other survivors along the way.",
      popularity: 1912.474,
      posterPath: '/xf9wuDcqlUPWABZNeDKPbZUjWx0.jpg',
      firstAirDate: '2010-10-31',
      name: 'The Walking Dead',
      voteAverage: 8.112,
      voteCount: 13929,
    },
    {
      adult: false,
      backdropPath: '/5vUux2vNUTqwCzb7tVcH18XnsF.jpg',
      genreIds: [18, 80],
      id: 113988,
      originCountry: ['US'],
      originalLanguage: 'en',
      originalName: 'Dahmer – Monster: The Jeffrey Dahmer Story',
      overview:
        'Across more than a decade, 17 teen boys and young men were murdered by convicted killer Jeffrey Dahmer. How did he evade arrest for so long?',
      popularity: 1996.447,
      posterPath: '/f2PVrphK0u81ES256lw3oAZuF3x.jpg',
      firstAirDate: '2022-09-21',
      name: 'Dahmer – Monster: The Jeffrey Dahmer Story',
      voteAverage: 8.212,
      voteCount: 1396,
    },
    {
      adult: false,
      backdropPath: '/1rO4xoCo4Z5WubK0OwdVll3DPYo.jpg',
      genreIds: [10765, 10759, 18],
      id: 84773,
      originCountry: ['US'],
      originalLanguage: 'en',
      originalName: 'The Lord of the Rings: The Rings of Power',
      overview:
        'Beginning in a time of relative peace, we follow an ensemble cast of characters as they confront the re-emergence of evil to Middle-earth. From the darkest depths of the Misty Mountains, to the majestic forests of Lindon, to the breathtaking island kingdom of Númenor, to the furthest reaches of the map, these kingdoms and characters will carve out legacies that live on long after they are gone.',
      popularity: 1921.695,
      posterPath: '/mYLOqiStMxDK3fYZFirgrMt8z5d.jpg',
      firstAirDate: '2022-09-01',
      name: 'The Lord of the Rings: The Rings of Power',
      voteAverage: 7.64,
      voteCount: 1431,
    },
    {
      adult: false,
      backdropPath: '/o8zk3QmHYMSC7UiJgFk81OFF1sc.jpg',
      genreIds: [10766, 18],
      id: 204095,
      originCountry: ['BR'],
      originalLanguage: 'pt',
      originalName: 'Mar do Sertão',
      overview: '',
      popularity: 796.614,
      posterPath: '/ixgnqO8xhFMb1zr8RRFsyeZ9CdD.jpg',
      firstAirDate: '2022-08-22',
      name: 'Mar do Sertão',
      voteAverage: 4.154,
      voteCount: 13,
    },
    {
      adult: false,
      backdropPath: '/rFcroHbhDFsuYtnHAgwU5q64YxV.jpg',
      genreIds: [18],
      id: 212041,
      originCountry: ['CN'],
      originalLanguage: 'zh',
      originalName: '我们这十年',
      overview: '',
      popularity: 479.807,
      posterPath: '/qN08pwJj4CHWgmXYtXf6jIIzciT.jpg',
      firstAirDate: '2022-10-10',
      name: '我们这十年',
      voteAverage: 5,
      voteCount: 1,
    },
    {
      adult: false,
      backdropPath: '/i9htchhoOl26cLaslVHXHghgLt0.jpg',
      genreIds: [10759, 16, 10762],
      id: 65334,
      originCountry: ['FR'],
      originalLanguage: 'fr',
      originalName: 'Miraculous, les aventures de Ladybug et Chat Noir',
      overview:
        'Normal high school kids by day, protectors of Paris by night! Miraculous follows the heroic adventures of Marinette and Adrien as they transform into Ladybug and Cat Noir and set out to capture akumas, creatures responsible for turning the people of Paris into villains. But neither hero knows the other’s true identity – or that they’re classmates!',
      popularity: 1399.252,
      posterPath: '/jThrG5IuMjbtMzhbNbQnKw5zQe5.jpg',
      firstAirDate: '2015-10-19',
      name: 'Miraculous: Tales of Ladybug & Cat Noir',
      voteAverage: 8.004,
      voteCount: 3848,
    },
  ],
  totalPages: 1,
  totalResults: 6,
};
