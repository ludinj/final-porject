import { useSearchParams } from 'react-router-dom';
import { EFilters } from '../ts/enums';
import { PageSelect } from '../ts/interfaces';

export const useChangeParams = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const currentPage = searchParams.get(EFilters.page) || '1';
  const genreQuery = searchParams.get(EFilters.genre);
  const yearQuery = searchParams.get(EFilters.year);
  const certificationQuery = searchParams.get(EFilters.certification);
  const query = searchParams.get(EFilters.query);

  const handlePageClick = (e: PageSelect) => {
    const page = e.selected + 1; //the selected number starts from 0
    searchParams.set(EFilters.page, page.toString());

    setSearchParams(searchParams);
  };

  const handleYearChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (!e.target.value) {
      searchParams.delete(EFilters.year);
      searchParams.delete(EFilters.page);
      setSearchParams(searchParams);
    } else {
      searchParams.set(EFilters.year, e.target.value.toString());
      searchParams.delete(EFilters.page);
      setSearchParams(searchParams);
    }
  };
  const handleChangeCertification = (
    e: React.ChangeEvent<HTMLSelectElement>
  ) => {
    if (!e.target.value) {
      searchParams.delete(EFilters.certification);
      searchParams.delete(EFilters.page);
      setSearchParams(searchParams);
    } else {
      searchParams.set(EFilters.certification, e.target.value);
      searchParams.delete(EFilters.page);
      setSearchParams(searchParams);
    }
  };

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.value) {
      searchParams.delete(EFilters.query);
      searchParams.delete(EFilters.page);
      setSearchParams(searchParams);
    } else {
      searchParams.set(EFilters.query, e.target.value);
      searchParams.delete(EFilters.page);
      setSearchParams(searchParams);
    }
  };
  return {
    handleYearChange,
    handleChangeCertification,
    handlePageClick,
    currentPage,
    genreQuery,
    yearQuery,
    certificationQuery,
    handleSearch,
    query,
  };
};
