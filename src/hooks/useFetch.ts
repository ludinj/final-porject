import { useState, useCallback } from 'react';
import axios from 'axios';
import camelCaseKeys from 'camelcase-keys';
import { EFilters } from '../ts/enums';

const axiosInstance = axios.create();
axiosInstance.interceptors.response.use(
  function (response) {
    return {
      ...response,
      data: camelCaseKeys(response.data, { deep: true }),
    };
  },
  function (error) {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.request.use((config) => {
  config.params = config.params || {};
  config.params[EFilters.api_key] = `${process.env.REACT_APP_API_KEY}`;
  return config;
});
export const useFetch = () => {
  const [error, setError] = useState<unknown>(null);
  const [loading, setLoading] = useState(false);

  const fetchData = useCallback(async (url: string) => {
    setLoading(true);

    try {
      const response = await axiosInstance.get(url);
      const data = response.data;
      setLoading(false);
      return data;
    } catch (error) {
      setLoading(false);
      setError(error);
    }
  }, []);

  return { error, loading, fetchData };
};
