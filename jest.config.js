const { compilerOptions } = require('./tsconfig.json');

const { pathsToModuleNameMapper } = require('ts-jest');
const { default: tsjPreset } = require('ts-jest');

module.exports = {
  preset: 'ts-jest',
  transform: {
    ...tsjPreset.transform,
    '+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2|pdf)$':
      'jest-transform-stub',
  },
  moduleDirectories: ['src', 'node_modules'],
  moduleNameMapper: {
    ...pathsToModuleNameMapper(compilerOptions.paths),
    '^axios$': require.resolve('axios'),
  },
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts'],
  transformIgnorePatterns: ['node_modules/(?!(axios)/)'],
};
